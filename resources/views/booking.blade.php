<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ Session::token() }}">

        <title>Book</title>
        @if (!Auth::check())
            <script>window.location = "/";</script>
        @endif

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <style>

            @media screen and (min-width: 870px) {
                .booking-container .booking-form{
                    width: 472px;
                    height: 382px;

                    background: #4A5D8D;
                    border-radius: 25px;
                }

                .form-input{
                    border-radius: 20px;
                    border: 0px;
                    width: 185px;
                    height: 36px;
                    background-color: #fff;
                    color: #000;
                }

                .form-input-description{
                    border-radius: 20px;
                    border: 0px;
                    width: 428px;
                    height: 36px;
                    background-color: #fff;
                    color: #000;
                }

                .btn.btn-primary{
                    border: 0px;
                    border-radius: 30px;
                    width: 134px;
                    margin: 5px;
                }

                .book-btn{
                    width: 120px;
                    height: 50px;

                    background: #5F9DFA;
                    border-radius: 86px;
                    margin-right: 50px;
                    margin-top: -30px;

                    font-family: Roboto;
                    font-style: normal;
                    font-weight: 500;
                    font-size: 20px;
                    line-height: 28px;

                    color: #FFFFFF;

                }
            }

            @media screen and (max-width: 870px) {
                .booking-container .booking-form{
                    width: 342px;
                    height: 400px;

                    background: #4A5D8D;
                    border-radius: 25px;
                }

                .form-input{
                    border-radius: 20px;
                    border: 0px;
                    width: 148px;
                    height: 29px;
                    font-size: 13px;
                    background-color: #fff;
                    color: #000;
                }

                .form-input-description{
                    border-radius: 20px;
                    border: 0px;
                    width: 301px;
                    height: 29px;
                    font-size: 13px;
                    background-color: #fff;
                    color: #000;
                }

                .btn.btn-primary{
                    border: 0px;
                    border-radius: 16px;
                    font-size: 13px;
                    width: 91px;
                    height: 50px;
                    margin: 5px;
                }

                .book-btn{
                    width: 92px;
                    height: 43px;

                    background: #5F9DFA;
                    border-radius: 86px;
                    margin-right: 50px;
                    margin-top: -21px;

                    font-family: Roboto;
                    font-style: normal;
                    font-weight: 500;
                    font-size: 18px;
                    line-height: 28px;

                    color: #FFFFFF;

                }
            }

            .booked-list{
                /* width: 727px; */
                height: 136px;

                background: #1D3062;
                border: 3px solid #1D3062;
                box-sizing: border-box;
                border-radius: 15px;
            }

            .book-title{
                font-family: Roboto;
                font-style: normal;
                color: #24233d;
                font-size: 20px;
                margin-top: 30px;
                margin-bottom: 20px;
            }

            .history-list{
                /* width: 727px; */
                height: 136px;

                background: #4A5D8D;
                border: 3px solid #1D3062;
                box-sizing: border-box;
                border-radius: 15px;
            }

            .history-title{
                font-family: Roboto;
                font-style: normal;
                color: #24233d;
                font-size: 20px;
                margin-top: 30px;
                margin-bottom: 20px;
            }

            .booking-container{
                width: fit-content;
                background: #414141;
                border-radius: 20px;
            }



            .custom-toggle{
                width: 160px;
                height: 36px;
                color: #4A5D8D;
                background: #FFFFFF;
                border-radius: 20px;
                border: 0px;
            }

            .btn.btn-primary.disabled{
                background-color: #414141;
                color: #5e5e5e;
                border: 0px;
            }

            .btn.btn-primary.selected{
                background-color: #e9bf05;
                color: #ffffff;
                border: 0px;
            }



        </style>

    </head>
    @php
        $userId = Auth::id();
    @endphp


    <body style="background-color: rgb(168, 172, 207)">
            <nav class="navbar navbar-expand-sm navbar-dark" style="background-color: #4A5D8D">
                <a class="navbar-brand" href="#"><img class="logo" src="http://sapsp.uum.edu.my/images/Logo-SAPSP_DarkBG.png" alt="profile Pic" height="30px"></a>
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse " id="collapsibleNavId">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('/home')}}">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Book</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="userSetting">Setting</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('/logout')}}">Logout</a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="row justify-content-center" style="margin:80px 0px">
                <div>
                    <div class="booking-container">
                        <img class="logo" style="margin:10px" src="http://sapsp.uum.edu.my/images/Logo-SAPSP_DarkBG.png" alt="profile Pic" height="100">

                        <form method="POST" action="/booked">
                            {{ csrf_field() }}
                            <div class="booking-form">

                                <div class="container" style="margin ">
                                    <div class="form-group float-left" style="margin: 19px 0px 0px 0px">
                                        <label class="login-label" style="font-size: 13px; color:#fff; margin-left:10px; margin-bottom:3px" for="date">Date</label>
                                        <input type="date" class="form-control form-input" id="date" placeholder="Date" name="date">
                                    </div>

                                    <div class="form-group float-right" style="margin: 19px 0px 0px 0px">
                                        <label class="login-label" for="labDropdown" style="font-size: 13px; color:#fff; margin-left:10px; margin-bottom:3px">Lab</label>
                                        <div class="dropdown">
                                            <button class="btn dropdown-toggle form-input" type="button" id="labDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" name="lab">Choose Lab</button>

                                            <div class="dropdown-menu" aria-labelledby="labDropdown">
                                                @foreach ($labs as $lab)
                                                    <a class="dropdown-item" onclick="selectedLab('{{$lab->name}}','{{$lab->id}}')" href="#">{{$lab->name}}</a>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>

                                    <div id="time-container" style="padding-top:110px ">
                                        @for($loop = 0; $loop <9; $loop++)
                                            <button class="btn btn-primary" id="{{substr($timeList[$loop],0,2)}}" onclick="selectTime('{{substr($timeList[$loop],0,2)}}')" type="button">{{$timeList[$loop]}}</button>
                                        @endfor

                                    </div>
                                    <div class="form-group" style="margin: 10px 0px 0px 5px;">
                                        <label class="login-label" style="font-size: 13px; color:#fff; margin-left:10px; margin-bottom:3px" for="date">Purpose</label>
                                        <input type="text" class="form-control input-login form-input-description" id="description" placeholder="Booking lab purpose" name="description">
                                    </div>


                                </div>
                            </div>
                        </form>

                    </div>
                    <button href="#" type="submit" class="btn book-btn float-right" onclick="submitBook()">Book</button>
                </div>

            </div>


    </body>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        var before = true;
        function getToday(){
            var today = new Date();
            var dd = String(today.getDate()).padStart(2,'0');
            var mm = String(today.getMonth()+1).padStart(2,'0');
            var yyyy = today.getFullYear();

            today = mm+'/'+dd+'/'+yyyy;
            return today;
        }

        function dayDifferent(current, selected){
            dt1 = new Date(current);
            dt2 = new Date(selected);
            return Math.floor((Date.UTC(dt2.getFullYear(),dt2.getMonth(),dt2.getDate()) - 
            Date.UTC(dt1.getFullYear(),dt1.getMonth(),dt1.getDate()) ) /(1000 * 60 * 60 * 24 ));
        }

        function selectedLab(labName,labId){
            document.getElementById("labDropdown").innerHTML = labName;
            var getLabNAme = document.getElementById('labDropdown').innerHTML;
            $.ajax({
                url: "{{url('/selected')}}",
                method: "GET",
                data: {date: $("#date").val(), lab: labName},
                success:function(data){
                    var time = JSON.parse(data);
                    var timeSize = Object.keys(time).length;
                    for(j = 8 ; j < 18; j++){
                        if(j<10){
                            $('#0'+j).removeClass('disabled');
                            $('#0'+j).removeClass('selected');
                        }else{
                            $('#'+j).removeClass('disabled');
                            $('#'+j).removeClass('selected');
                        }
                    }
                    for(i = 0; i < timeSize; i++){
                        for(j = 8 ; j < 18; j++){
                            if(j == parseInt(time[i].book_time.substring(0,2))){
                                $('#'+time[i].book_time.substring(0,2)).toggleClass('disabled');
                            }
                        }
                    }
                }
            })
        }

        function selectTime(timeId){
            if(!$('#'+timeId).hasClass('disabled')){
                if($('#'+timeId).hasClass('selected')){
                $('#'+timeId).removeClass('selected');
                }
                else{
                    $('#'+timeId).addClass('selected');
                }
            }

        }

        function submitBook(){
            var getLabNAme = document.getElementById('labDropdown').innerHTML;
            
            if($('#date') != 'mm/dd/yyyy' && getLabNAme != 'Choose Lab' && $("#description").val() != "" && before){
                for( i = 8 ; i < 18 ; i++){
                    if(i<10){
                        if($('#0'+i).hasClass('selected')){
                            $.post("/booked",{
                                '_token': $('meta[name=csrf-token]').attr('content'),
                                user_id: "{{$userId}}",
                                user_name: "{{Auth::user()->name}}",
                                lab_name: getLabNAme,
                                book_date: $("#date").val(),
                                book_time: "0"+i+":00:00",
                                description: $('#description').val(),
                            },function(data,status){
                                if(status === 'success'){
                                    alert("Successfully book \nLab : "+getLabNAme+"\nDate : "+ $("#date").val()+ "\nTime : "+data+":00");
                                    $('#'+data).removeClass('selected');
                                    $('#'+data).addClass('disabled');

                                    $.post("send-mail",{
                                        '_token': $('meta[name=csrf-token]').attr('content'),
                                        id: "{{$userId}}",
                                        name: "{{Auth::user()->name}}",
                                        lab: getLabNAme,
                                        date: $("#date").val(),
                                        time: i+":00",
                                        description: $('#description').val(),
                                    },function(data,status){});
                                }
                            })
                        }
                    }else{
                        if($('#'+i).hasClass('selected')){
                            $.post("/booked",{
                                '_token': $('meta[name=csrf-token]').attr('content'),
                                user_id: "{{$userId}}",
                                user_name: "{{Auth::user()->name}}",
                                lab_name: getLabNAme,
                                book_date: $("#date").val(),
                                book_time: i+":00:00",
                                description: $('#description').val(),
                            },function(data,status){
                                if(status === 'success'){
                                    alert("Your request has request to book \nLab : "+getLabNAme+"\nDate : "+ $("#date").val()+ "\nTime : "+data+":00");
                                    $('#'+data).removeClass('selected');
                                    $('#'+data).addClass('disabled');

                                    $.post("send-mail",{
                                        '_token': $('meta[name=csrf-token]').attr('content'),
                                        id: "{{$userId}}",
                                        name: "{{Auth::user()->name}}",
                                        lab: getLabNAme,
                                        date: $("#date").val(),
                                        time: i+":00",
                                        description: $('#description').val(),
                                    },function(data,status){});
                                }

                            })
                        }
                    }
                }
            }
            else{
                alert("Please fill in all input field and make sure the booking is for 2 days after today");
            }

        }

    </script>
    <script>
            $('#date').change(function(e){
                e.preventDefault();
                var getLabNAme = document.getElementById('labDropdown').innerHTML;

                var gDay = $('#date').val().substring(8,10);
                var gMonth = $("#date").val().substring(5,7);
                var gYear = $("#date").val().substring(0,4);
                var gDate = gMonth+'/'+gDay+'/'+gYear;

                if( dayDifferent(getToday(),gDate) < 3){
                    alert("Lab booking must be done for 2 days before the selected date");
                    before = false;
                }
                else{
                    before = true;
                    if(getLabNAme != 'Choose Lab'){
                        $.ajax({
                            url: "{{url('/selected')}}",
                            method: "GET",
                            data: {date: $("#date").val(), lab: getLabNAme},
                            success:function(data){
                                var time = JSON.parse(data);
                                var timeSize = Object.keys(time).length;
                                for(j = 8 ; j < 18; j++){
                                    if(j<10){
                                        $('#0'+j).removeClass('disabled');
                                        $('#0'+j).removeClass('selected');
                                    }else{
                                        $('#'+j).removeClass('disabled');
                                        $('#'+j).removeClass('selected');
                                    }
                                }
                                for(i = 0; i < timeSize; i++){
                                    for(j = 8 ; j < 18; j++){
                                        if(j == parseInt(time[i].book_time.substring(0,2))){
                                            $('#'+time[i].book_time.substring(0,2)).toggleClass('disabled');
                                        }
                                    }
                                    $('#teseract').append(time[i].book_time.substring(0,2));
                                }
                            }
                        })
                    }
                }

                

            })



    </script>
</html>
