<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Welcome to SAPSP Booking</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <style>

            @media screen and (min-width: 870px) {
                .title{
                    font-family: Roboto;
                    font-style: normal;
                    font-weight: 550;
                    font-size: 70px;
                    line-height: 70px;
                    max-width: 320px;
                    padding-top: 35px;
                    padding-left: 70px;
                    color: #ffffff;
                }

                .logo{
                    margin: 50px;
                }

                .login-box{
                    left: 793px;
                    top: 259px;
                    max-width: 500px;
                    min-width: 420px;
                    background: #FFFFFF;
                    border-radius: 40px;
                    padding-top: 50px;
                    padding-left: 50px;
                    padding-right: 50px;
                    padding-bottom: 70px;
                }
            }

            @media screen and (max-width: 870px) {
                .title{
                    font-family: Roboto;
                    font-style: normal;
                    font-weight: 550;
                    font-size: 35px;
                    line-height: 45px;
                    margin: 20px;
                    color: #ffffff;
                }

                .logo{
                    margin: 30px;
                    height: 100px;
                }

                .login-box{
                    left: 793px;
                    top: 259px;
                    min-width: 330px;
                    background: #FFFFFF;
                    border-radius: 40px;
                    padding: 30px 30px 50px 30px;
                }
            }

            body{
                background-color:#4A5D8D;
            }






            .login-btn{
                width: 150px;
                height: 60px;

                background: #5F9DFA;
                border-radius: 86px;
                margin-right: 50px;
                margin-top: -30px;

                font-family: Roboto;
                font-style: normal;
                font-weight: 500;
                font-size: 20px;
                line-height: 28px;

                color: #FFFFFF;

            }

            .login-container{
                align: center;
                padding: 30px;
                max-width:500px;
                margin-bottom: 35px;
            }

            .login-input{
                background: #E6E6E6;
                border-radius: 15px;
                border: 0px;
                /* max-width: 406px; */
                height: 43px;
                font-family: Roboto;
                font-style: normal;
                font-weight: 550;
                font-size: 16px;
            }

            .login-label{
                font-family: Roboto;
                font-style: normal;
                font-weight: 400;
                font-size: 15px;
                color: #1D326A;
            }

            .row{
                margin: 0px;
            }

        </style>

    </head>
    <body>

        <div class="flex-center position-ref full-height">
            <div>
                <img class="logo" src="http://sapsp.uum.edu.my/images/Logo-SAPSP_DarkBG.png" alt="profile Pic" height="150">
            </div>
            <div class="row justify-content-center">
                <div class="container col">
                    <div class="title container">
                        WELCOME to SAPSP LAB BOOKING
                    </div>
                </div>

                <div class="container col">

                    <!-- Login -->
                    <div class="login-container" id="loginBox">
                        <form method="POST" action="/login">
                            {{ csrf_field() }}
                            <div class="login-box justify-content-center">
                                <div class="form-group">
                                    <label class="login-label" for="inputMatrixNumber">Matrix Number</label>
                                    <input type="number" class="form-control login-input" id="inputMatrixNumber" placeholder="Enter matrix number" name="id">
                                </div>
                                <div class="form-group">
                                    <label class="login-label" for="inputPassword1">Password</label>
                                    <input type="password" class="form-control login-input" id="inputPassword1" placeholder="Password" name="password">
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <a href="#" class="float-right" id="signup" onClick="$('#loginBox').hide(); $('#signupBox').show()">Sign Up Here</a>
                                    <p class="float-right" style="margin-right: 8px">First time user?  </p>
                                </div>
                            </div>
                            <button href="#" type="submit" class="btn login-btn float-right">Login</button>
                        </form>
                    </div>

                    <!-- Register -->
                    <div class="login-container" id="signupBox" style="display:none">
                        <form method="POST" action="/register">
                        {{ csrf_field() }}
                            <div class="login-box justify-content-center">

                                <div class="form-group">
                                    <label class="login-label" for="matrixNumber">Matrix Number</label>
                                    <input type="number" class="form-control login-input" id="matrixNumber" placeholder="Enter matrix number" name="id">

                                </div>
                                <div class="form-group">
                                    <label class="login-label" for="nameInput">Name</label>
                                    <input type="text" class="form-control login-input" id="nameInput" placeholder="Enter name" name="name">
                                </div>
                                <div class="form-group">
                                    <label class="login-label" for="emailInput">Email</label>
                                    <input type="email" class="form-control login-input" id="emailInput" placeholder="Enter email" name="email">
                                </div>
                                <div class="form-group">
                                    <label class="login-label" for="contactInput">Phone no.</label>
                                    <input type="tel" class="form-control login-input" id="contactInput" placeholder="Enter phone no." name="contact">
                                </div>
                                <div class="form-group">
                                    <label class="login-label" for="inputPassword2">Password</label>
                                    <input type="password" class="form-control login-input" id="inputPassword2" placeholder="Password" name="password">
                                </div>
                                <div class="form-group">
                                    <label class="login-label" for="inputRetypePassword2">Retype Password</label>
                                    <input type="password" class="form-control login-input" id="inputRetypePassword2" placeholder="Retype password">
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <a href="#" class="float-right" id="signup" onClick="$('#signupBox').hide(); $('#loginBox').show()">Sign In Here</a>
                                </div>
                            </div>
                            <button href="#" type="submit" class="btn login-btn float-right">Sign Up</button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </body>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    @if (Session::has('alert'))
        <script>
            alert("{{ session()->get('alert') }}");
        </script>
    @endif
</html>
