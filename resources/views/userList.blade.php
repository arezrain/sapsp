<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ Session::token() }}">

        <title>SAPSP Booking System</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />

        @php
            $superAdmin = false;
            if(Auth::user()->role == 0){
                $superAdmin = true;
            }
        @endphp

        <style>

            @media screen and (min-width: 870px) {
            }

            @media screen and (max-width: 870px) {
            }

            @media screen and (max-width: 1570px) {
              .welcome-text{
                margin-top: 0;
                margin-right: 30px;
                margin-bottom: 60px;
              }
            }

            @media screen and (min-width: 1570px) {
              .welcome-text{
                margin: 30px;
              }
            }

            .custom-nav {
                background: #1D3062;
                margin: 0px;
            }

            .logo {
                margin: 30px 26px 40px 20px;
            }



            .right-text{
                margin-right: 0px;
                padding-right: 0px;
                text-align: right;
                color: #ffffff;
                position: relative;
            }

            .right-nav{
                position: absolute;
                bottom: 0;
                right: 0;
            }

            .active{
                background-color: #0E1F4D;
            }

            .nav-link{
                color: #ffffff;
                width: 100px;
                text-align: center;
            }

            a:hover{
                color: #8ACCE9;
            }

            .dropdown-menu{
                background-color: #1D3062;
            }

            .dropdown-item{
                color: #fff;
            }

            .dropdown-menu a:hover{
                background-color: #0c193d;
                color: #fff;
            }

            .dropdown-item.active{
                background-color: #0E1F4D;
                color: #fff;
            }

            table {
                text-align: center;
            }

        </style>

    </head>
    @php

    @endphp


    <body >

        <div class="custom-nav row">
            <div>
                <img class="logo" src="http://sapsp.uum.edu.my/images/Logo-SAPSP_DarkBG.png" alt="profile Pic" height="140px">
            </div>
            <div class="container right-text">
                <h2 class="welcome-text">SAPSP BOOKING SYSTEM</h2>
                <nav class="nav float-right right-nav">
                    <a class="nav-link" href="/sapsp-admin">Home</a>
                    <a class="nav-link" href="book">Book</a>
                    <a class="nav-link" href="adminRequest">Request</a>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle active" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Admin</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="adminLog">Logs</a>
                            @if (Auth::user()->role === 0)
                                <a class="dropdown-item" href="adminLab">Labs</a>
                            @endif                            
                            <a class="dropdown-item active" href="#">Users</a>
                            <a class="dropdown-item" href="userSetting">Setting</a>
                            <a class="dropdown-item"href="adminLogout">Logout</a>
                        </div>
                    </li>
                </nav>
            </div>
        </div>

        <div class="container">
            <div class="form-group float-left" style="max-width: 230px; margin-top: 20px">
                <label for="userSearch">Search ID</label>
                <input type="text" class="form-control" id="userSearch" placeholder="Enter ID" name="search">
            </div>
            @if($superAdmin === true)
                <button type="button" class="btn btn-primary float-right" style="margin: 50px 50px 30px 50px" data-toggle="modal" data-target="#addModal">Add Admin</button>
            @endif

            <table id="labsTable" class="table" style="margin-top: 20px">
                <thead>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Contact</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Date Created</th>
                        <th>Date Modified</th>
                        <th>Action</th>
                </thead>
                <tbody id="userBody">
                    @foreach ($users as $user)
                        <tr>
                            <th>{{$user->id}}</th>
                            <td>{{ $user->name }}</td>
                            <td>{{$user->contact}}</td>
                            <td>{{$user->email}}</td>
                            @if ($user->role === 1)
                                <td>Admin</td>
                            @elseif ($user->role === 0)
                                <td>Super Admin</td>
                            @else
                                <td>User</td>
                            @endif
                            <td>{{ substr($user->created_at,0,16) }}</td>
                            <td>{{ substr($user->updated_at,0,16) }}</td>
                            <td>
                                <div class="container">
                                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#resetModal" onclick="getReset('{{ $user->id }}')">reset password</button>
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal" onclick="getDelete('{{ $user->id }}')">delete</button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>



        <div class="modal fade" id="resetModal" tabindex="-1" role="dialog" aria-labelledby="resetModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="resetModalLabel">Confirmation</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="POST" action="/editLab">
                    <div class="modal-body">
                        Are you sure want to reset this user password?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" onclick="resetFunc()">Confirm</button>
                    </div>
                </form>
              </div>
            </div>
          </div>

          <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="deleteModalLabel">Confirmation</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="POST" action="/addLab">
                    <div class="modal-body">
                        Are you sure want to delete this user account?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" onclick="deleteFunc()">Confirm</button>
                    </div>
                </form>
              </div>
            </div>
          </div>

          <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="addModalLabel">Add Admin User</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="POST" action="/addAdmin">
                    <div class="modal-body">
                        <div class="container">


                            {{ csrf_field() }}
                            <div class="login-box justify-content-center">

                                <div class="form-group">
                                    <label for="addId">Staff ID</label>
                                    <input type="number" class="form-control" id="addId" placeholder="Enter staff ID" name="id">
                                  </div>
                                  <div class="form-group">
                                      <label for="addName">Name</label>
                                      <input type="text" class="form-control" id="addName" placeholder="Enter full name" name="name">
                                  </div>
                                  <div class="form-group">
                                      <label for="addEmail">Email</label>
                                      <input type="email" class="form-control" id="addEmail" placeholder="Enter email" name="email">
                                  </div>
                                  <div class="form-group">
                                      <label for="addContact">Phone no.</label>
                                      <input type="tel" class="form-control" id="addContact" placeholder="Enter contact no" name="contact">
                                    </div>
                                    <div class="form-group">
                                        <label for="roleSelect">Role</label>
                                        <select id="roleSelect" name="role" class="custom-select">
                                            <option value="1">Admin</option>
                                            <option value="0">Super Admin</option>
                                        </select>
                                    </div>
                                    
                                  <div class="form-group">
                                    <label for="addPassword">Password</label>
                                    <input type="password" class="form-control" id="addPassword" placeholder="password" name="password">
                                  </div>
                                  <div class="form-group">
                                      <label for="addCheckPassword">Retype password</label>
                                      <input type="password" class="form-control" id="addCheckPassword" placeholder="Retype password" name="checkPassword">
                                    </div>
                            </div>


                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" onclick="addFunc()">Submit</button>
                    </div>
                </form>
              </div>
            </div>
          </div>
    </body>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
    <script>

        var getId = 0;

        function getDelete(id){
            getId = id;
        }

        function getReset(id){
            getId = id;
        }

        function resetFunc(){

            $.post("/resetUser",{
                                '_token': $('meta[name=csrf-token]').attr('content'),
                                id: getId,
                            },function(data,status){
                                if(status === 'success'){
                                    location.reload();
                                }
                            });
        }

        function deleteFunc(){

            $.post("/deleteUser",{
                                '_token': $('meta[name=csrf-token]').attr('content'),
                                id: getId,
                            },function(data,status){
                                if(status === 'success'){
                                    location.reload();
                                }
                            });
        }

        function addFunc(){
            $.post("/addAdmin",{
                                '_token': $('meta[name=csrf-token]').attr('content'),
                                id: $("#addId").val(),
                                name: $("#addName").val(),
                                email: $("#addEmail").val(),
                                contact: $("#addContact").val(),
                                role: $("#roleSelect").val(),
                                password: $("#addPassword").val(),
                                checkPassword: $("#addCheckPassword").val(),
                            },function(data,status){
                                if(data == 'success'){
                                    location.reload();
                                }
                                else if(data == 'exist'){
                                    alert("user Id already existed");
                                }
                                else if(data == 'different'){
                                    alert("Password is not same with retype password");
                                }
                            });
        }

        $("#userSearch").keyup(function(){
            $.post("/admin/search",{
                                '_token': $('meta[name=csrf-token]').attr('content'),
                                id: $("#userSearch").val(),
                            },function(data,status){
                                if(status == 'success'){
                                    $("#userBody").empty();
                                    var jsonData = JSON.parse(data);
                                    // alert(jsonData);

                                    for(var i = 0; i < Object.keys(jsonData).length;i++){
                                        var date = jsonData[i].created_at;
                                        var update = jsonData[i].updated_at;
                                        var role = "User";
                                        if(jsonData[i].role == 0){
                                            role = "Super Admin";
                                        }
                                        else if(jsonData[i].role == 1){
                                            role = "Admin";
                                        }
                                        $("#userBody").append('<tr><th>'+jsonData[i].id+'</th><td>'+
                                        jsonData[i].name+'</td><td>'+jsonData[i].contact+'</td><td>'+jsonData[i].email+
                                        '</th><td>'+role+'</td><td>'+
                                        date.substring(0,10)+' '+date.substring(11,16)+'</td><td>'+
                                        update.substring(0,10)+' '+update.substring(11,16)+'</td><td>'+
                                        '<div class="container">'+
                                        '<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#resetModal" onclick="getReset('+jsonData[i].id+')">reset password</button>'+
                                        '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal" onclick="getDelete('+jsonData[i].id+')">delete</button>'+
                                        '</div></td></tr>');
                                    }
                                }
                            });
        });
    </script>
</html>
