<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ Session::token() }}">

        <title>SAPSP Booking System</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />


        <style>

            @media screen and (min-width: 870px) {
            }

            @media screen and (max-width: 870px) {
            }

            @media screen and (max-width: 1570px) {
              .welcome-text{
                margin-top: 0;
                margin-right: 30px;
                margin-bottom: 60px;
              }
            }

            @media screen and (min-width: 1570px) {
              .welcome-text{
                margin: 30px;
              }
            }

            .custom-nav {
                background: #1D3062;
                margin: 0px;
            }

            .logo {
                margin: 30px 26px 40px 20px;
            }



            .right-text{
                margin-right: 0px;
                padding-right: 0px;
                text-align: right;
                color: #ffffff;
                position: relative;
            }

            .right-nav{
                position: absolute;
                bottom: 0;
                right: 0;
            }

            .active{
                background-color: #0E1F4D;
            }

            .nav-link{
                color: #ffffff;
                width: 100px;
                text-align: center;
            }

            a:hover{
                color: #8ACCE9;
            }

            .dropdown-menu{
                background-color: #1D3062;
            }

            .dropdown-item{
                color: #fff;
            }

            .dropdown-menu a:hover{
                background-color: #0c193d;
                color: #fff;
            }

            .dropdown-item.active{
                background-color: #0E1F4D;
                color: #fff;
            }

            table {
                text-align: center;
            }

        </style>

    </head>
    @php

    @endphp


    <body >
        <div class="custom-nav row">
            <div>
                <img class="logo" src="http://sapsp.uum.edu.my/images/Logo-SAPSP_DarkBG.png" alt="profile Pic" height="140px">
            </div>
            <div class="container right-text">
                <h2 class="welcome-text">SAPSP BOOKING SYSTEM</h2>
                <nav class="nav float-right right-nav">
                    <a class="nav-link" href="/sapsp-admin">Home</a>
                    <a class="nav-link" href="book">Book</a>
                    <a class="nav-link" href="adminRequest">Request</a>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle active" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Admin</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="adminLog">Logs</a>
                            <a class="dropdown-item active" href="#">Labs</a>
                            <a class="dropdown-item" href="userList">Users</a>
                            <a class="dropdown-item" href="userSetting">Setting</a>
                            <a class="dropdown-item" href="adminLogout">Logout</a>
                        </div>
                    </li>
                </nav>
            </div>
        </div>

        <div class="container">
            <button type="button" class="btn btn-primary float-right" style="margin: 50px 50px 30px 50px" data-toggle="modal" data-target="#addModal">Add Lab</button>
            <table id="labsTable" class="table">
                <tr>
                    <th>ID</th>
                    <th>Lab Name</th>
                    <th>Date Created</th>
                    <th>Date Modified</th>
                    <th>Action</th>
                </tr>
                @foreach ($labs as $lab)
                    <tr>
                        <th>{{$lab->id}}</th>
                        <td>{{ $lab->name }}</td>
                        <td>{{ $lab->created_at }}</td>
                        <td>{{ $lab->updated_at }}</td>
                        <td>
                            <div class="container">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal" onclick="getVal('{{ $lab->id }}','{{ $lab->name }}')">edit</button>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal" onclick="setId({{$lab->id}})">delete</button>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>



        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="editModalLabel">Edit Lab</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="POST" action="/editLab">
                    <div class="modal-body">
                        <div class="container">


                            {{ csrf_field() }}
                            <div class="login-box justify-content-center">

                                <div class="form-group">
                                    <label class="login-label" for="labID">Lab ID</label>
                                    <input type="number" class="form-control login-input" id="labID" name="id">

                                </div>
                                <div class="form-group">
                                    <label class="login-label" for="nameInput">Lab Name</label>
                                    <input type="text" class="form-control login-input" id="nameInput" name="name">
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" onclick="editFunc()">Save changes</button>
                    </div>
                </form>
              </div>
            </div>
        </div>

        <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="addModalLabel">Add Lab</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="POST" action="/addLab">
                    <div class="modal-body">
                        <div class="container">


                            {{ csrf_field() }}
                            <div class="login-box justify-content-center">
                                <div class="form-group">
                                    <label class="login-label" for="addNameInput">Lab Name</label>
                                    <input type="text" class="form-control login-input" id="addNameInput" name="name">
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" onclick="addFunc()">Save changes</button>
                    </div>
                </form>
              </div>
            </div>
          </div>

          <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="deleteModalLabel">Confirmation</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="POST" action="/addLab">
                    <div class="modal-body">
                        Are you sure want to delete this lab?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" onclick="deleteFunc()" data-dismiss="modal">Confirm</button>
                    </div>
                </form>
              </div>
            </div>
          </div>

    </body>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
    <script>

        var oldId = 0;
        var delId = 0;

        function setId(id){
            delId = id;
        }

        function getVal(id,name){
            $("#labID").val(id);
            $("#nameInput").val(name);
            oldId = id;
        }

        function editFunc(){

            $.post("/editLab",{
                                '_token': $('meta[name=csrf-token]').attr('content'),
                                oldId: oldId,
                                newId: $("#labID").val(),
                                newName: $("#nameInput").val(),
                            },function(data,status){
                                if(status === 'success'){
                                    location.reload();
                                }
                            });
        }

        function addFunc(){

            $.post("/addLab",{
                                '_token': $('meta[name=csrf-token]').attr('content'),
                                newName: $("#addNameInput").val(),
                            },function(data,status){
                                if(status === 'success'){
                                    location.reload();
                                }
                            });
        }

        function deleteFunc(){

            $.post("/deleteLab",{
                                '_token': $('meta[name=csrf-token]').attr('content'),
                                id: delId,
                            },function(data,status){
                                if(status === 'success'){
                                    location.reload();
                                }
                            });
            }
    </script>
</html>
