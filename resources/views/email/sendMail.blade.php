<!DOCTYPE html>
<html>
<head>
    <title>Booking Notification</title>
</head>
<body>
    <h3>{{ $details['title'] }}</h3>
    <p>{{ $details['body'] }}</p>
    <p>{{ $details['lab'] }}</p>
    <p>{{ $details['id'] }}</p>
    <p>{{ $details['name'] }}</p>
    <p>{{ $details['contact'] }}</p>
    <p>{{ $details['email'] }}</p>
    <p>{{ $details['date'] }}</p>
    <p>{{ $details['time'] }}</p>
    <p>Thank you</p>
</body>
</html>
