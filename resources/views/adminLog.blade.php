<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ Session::token() }}">

        <title>SAPSP Booking System</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />


        <style>

            @media screen and (min-width: 870px) {
            }

            @media screen and (max-width: 870px) {
            }

            @media screen and (max-width: 1570px) {
              .welcome-text{
                margin-top: 0;
                margin-right: 30px;
                margin-bottom: 60px;
              }
            }

            @media screen and (min-width: 1570px) {
              .welcome-text{
                margin: 30px;
              }
            }

            .custom-nav {
                background: #1D3062;
                margin: 0px;
            }

            .logo {
                margin: 30px 26px 40px 20px;
            }



            .right-text{
                margin-right: 0px;
                padding-right: 0px;
                text-align: right;
                color: #ffffff;
                position: relative;
            }

            .right-nav{
                position: absolute;
                bottom: 0;
                right: 0;
            }

            .active{
                background-color: #0E1F4D;
            }

            .nav-link{
                color: #ffffff;
                width: 100px;
                text-align: center;
            }

            a:hover{
                color: #8ACCE9;
            }

            .dropdown-menu{
                background-color: #1D3062;
            }

            .dropdown-item{
                color: #fff;
            }

            .dropdown-menu a:hover{
                background-color: #0c193d;
                color: #fff;
            }

            .dropdown-item.active{
                background-color: #0E1F4D;
                color: #fff;
            }

            .table td{
                border-left: 1px solid #aaaaaa;
                border-right: 1px solid #aaaaaa;
                border-bottom: 1px solid #aaaaaa;
            }

            .table th{
                border-left: 1px solid #aaaaaa;
                border-right: 1px solid #aaaaaa;
            }

        </style>

    </head>
    @php

    @endphp


    <body >
        <div class="custom-nav row">
            <div>
                <img class="logo" src="http://sapsp.uum.edu.my/images/Logo-SAPSP_DarkBG.png" alt="profile Pic" height="140px">
            </div>
            <div class="container right-text">
                <h2 class="welcome-text">SAPSP BOOKING SYSTEM</h2>
                <nav class="nav float-right right-nav">
                    <a class="nav-link" href="/sapsp-admin">Home</a>
                    <a class="nav-link" href="book">Book</a>
                    <a class="nav-link" href="adminRequest">Request</a>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle active" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Admin</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item active" href="#">Logs</a>
                            @if (Auth::user()->role === 0)
                                <a class="dropdown-item" href="adminLab">Labs</a>
                            @endif                            
                            <a class="dropdown-item" href="userList">Users</a>
                            <a class="dropdown-item" href="userSetting">Setting</a>
                            <a class="dropdown-item"href="adminLogout">Logout</a>
                        </div>
                    </li>
                </nav>
            </div>
        </div>

        <div class="container">
            <div class="dropdown open" style="margin: 10px">
                <button class="btn btn-secondary dropdown-toggle" style="width: 100px" type="button" id="viewType" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                            Month
                        </button>
                <div class="dropdown-menu" aria-labelledby="viewType">
                    <button class="dropdown-item" onclick="monthClick()" id="monthDrop" href="#">Month</button>
                    <button class="dropdown-item" onclick="dayClick()" id="dayDrop" href="#">Day</button>
                </div>
            </div>
            <input type="month" class="btn btn-primary" style="color: white; margin:10px" name="month" id="monthInput">
            <input type="date" class="btn btn-primary" style="margin:10px; color:white;" name="date" id="dateInput">
        </div>

        <div class="container">
            <table class="table" style="margin:15px 15px 30px 15px">
                <thead id="countHead">
                </thead>
                <tbody id="countBody">
                </tbody>
            </table>
        </div>

        <div class="container">
            <table class="table" style="margin:30px 15px 50px 15px">
                <thead>
                    <th>Lab</th>
                    <th>User Name</th>
                    <th>Purpose</th>
                    <th>Contact</th>
                    <th>Book Date</th>
                    <th>Book Time</th>
                </thead>
                <tbody id="list">
                </tbody>

            </table>
        </div>

    </body>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
    <script>

        $(document).ready(function start(){
            $("#dateInput").hide();
            initiate();
        })

        function monthClick(){
            $("#viewType").html("Month");
            $("#monthInput").show();
            $("#dateInput").hide();
        }

        function dayClick(){
            $("#viewType").html("Day");
            $("#monthInput").hide();
            $("#dateInput").show();
        }

        $("#dateInput").change(function test(){
            // alert($("#dateInput").val());
            $.post("/summaryDate",{
                                '_token': $('meta[name=csrf-token]').attr('content'),
                                date: $("#dateInput").val(),
                            },function(data,status){
                                if(status === 'success'){
                                    var jsonData = JSON.parse(JSON.stringify(data));
                                    // var labCount = jsonData.labCount;
                                    $("#list").empty();
                                    $("#countHead").empty();
                                    $("#countHead").append('<th>Lab</th>');
                                    for(var i = 0; i < Object.keys(jsonData.labcount).length; i++){
                                        $("#countHead").append('<th>'+jsonData.labcount[i].name+'</th>');
                                    }
                                    $("#countBody").empty();
                                    $("#countBody").append('<th>Total</th>');
                                    for(var i = 0; i < Object.keys(jsonData.labcount).length; i++){
                                        $("#countBody").append('<td>'+jsonData.labcount[i].total+'</td>');
                                    }
                                    for(var i = 0; i < Object.keys(jsonData.listlab).length; i++){
                                        $("#list").append('<tr><th>'+jsonData.listlab[i].name+'</th><td>'
                                        +jsonData.listlab[i].user_name+'</td><td>'+jsonData.listlab[i].description+'</td><td>'+jsonData.listlab[i].contact
                                        +'</td><td>'+jsonData.listlab[i].book_date+'</td><td>'+jsonData.listlab[i].book_time
                                        +'</td></tr>');
                                    }
                                }
                            });
        })

        $("#monthInput").change(function test(){
            // alert($("#monthInput").val());
            $.post("/summaryDate",{
                                '_token': $('meta[name=csrf-token]').attr('content'),
                                date: $("#monthInput").val(),
                            },function(data,status){
                                if(status === 'success'){
                                    var jsonData = JSON.parse(JSON.stringify(data));
                                    // var labCount = jsonData.labCount;
                                    $("#list").empty();
                                    $("#countHead").empty();
                                    $("#countHead").append('<th>Lab</th>');
                                    for(var i = 0; i < Object.keys(jsonData.labcount).length; i++){
                                        $("#countHead").append('<th>'+jsonData.labcount[i].name+'</th>');
                                    }
                                    $("#countBody").empty();
                                    $("#countBody").append('<th>Total</th>');
                                    for(var i = 0; i < Object.keys(jsonData.labcount).length; i++){
                                        $("#countBody").append('<td>'+jsonData.labcount[i].total+'</td>');
                                    }
                                    for(var i = 0; i < Object.keys(jsonData.listlab).length; i++){
                                        if(i == 0){
                                            $("#list").append('<tr><th>'+jsonData.listlab[i].name+'</th><td>'+jsonData.listlab[i].user_name+'</td><td>'+jsonData.listlab[i].description+'</td><td>'+jsonData.listlab[i].contact+'</td><td>'+jsonData.listlab[i].book_date+'</td><td>'+jsonData.listlab[i].book_time+'</td></tr>');

                                        }
                                        else{
                                            if(jsonData.listlab[i].name == jsonData.listlab[i-1].name){
                                                $("#list").append('<tr><th style="border-top:0px"></th><td>'+jsonData.listlab[i].user_name+
                                                '</td><td>'+jsonData.listlab[i].description+'</td><td>'+jsonData.listlab[i].contact+
                                                '</td><td>'+jsonData.listlab[i].book_date+'</td><td>'+jsonData.listlab[i].book_time+'</td></tr>');

                                            }else{
                                                $("#list").append('<tr><th>'+jsonData.listlab[i].name+'</th><td>'+jsonData.listlab[i].user_name+
                                                '</td><td>'+jsonData.listlab[i].description+'</td><td>'+jsonData.listlab[i].contact+
                                                '</td><td>'+jsonData.listlab[i].book_date+'</td><td>'+jsonData.listlab[i].book_time+'</td></tr>');
                                            }
                                        }
                                    }
                                }
                            });
        })

        function initiate(){
            var data = JSON.parse(JSON.stringify({!! $jsonList !!}));

            $("#list").empty();
            $("#countHead").empty();
            $("#countHead").append('<th>Lab</th>');
            for(var i = 0; i < Object.keys(data.labcount).length; i++){
                $("#countHead").append('<th>'+data.labcount[i].name+'</th>');
            }
            $("#countBody").empty();
            $("#countBody").append('<th>Total</th>');
            for(var i = 0; i < Object.keys(data.labcount).length; i++){
                $("#countBody").append('<td>'+data.labcount[i].total+'</td>');
            }
            for(var i = 0; i < Object.keys(data.listlab).length; i++){
                if(i == 0){
                    $("#list").append('<tr><th>'+data.listlab[i].name+'</th><td>'+
                    data.listlab[i].user_name+'</td><td>'+data.listlab[i].description+'</td><td>'+
                    data.listlab[i].contact+'</td><td>'+data.listlab[i].book_date+'</td><td>'+
                    data.listlab[i].book_time+'</td></tr>');

                }
                else{
                    if(data.listlab[i].name == data.listlab[i-1].name){
                        $("#list").append('<tr><th style="border-top:0px"></th><td>'+
                        data.listlab[i].user_name+'</td><td>'+data.listlab[i].description+'</td><td>'+
                        data.listlab[i].contact+'</td><td>'+data.listlab[i].book_date+'</td><td>'+
                        data.listlab[i].book_time+'</td></tr>');

                    }else{
                        $("#list").append('<tr><th>'+data.listlab[i].name+'</th><td>'+
                        data.listlab[i].user_name+'</td><td>'+data.listlab[i].description+'</td><td>'+
                        data.listlab[i].contact+'</td><td>'+data.listlab[i].book_date+
                        '</td><td>'+data.listlab[i].book_time+'</td></tr>');
                    }
                }
            }

        }
        // var data = JSON.parse(JSON.stringify({!! $jsonList !!}));
        // alert(data.listlab);
    </script>
</html>
