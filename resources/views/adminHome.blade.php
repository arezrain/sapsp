<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ Session::token() }}">

        <title>SAPSP Booking System</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />


        <style>

            @media screen and (min-width: 870px) {
                .p-5{
                    padding: 3rem!important;
                }

                .logo {
                    margin: 30px 26px 40px 20px;
                    height: 140;
                }
            }

            @media screen and (max-width: 870px) {
                .p-5{
                    padding: 1rem!important;
                }

                .logo {
                    margin: 30px 26px 40px 20px;
                    height: 93px;
                }
            }

            @media screen and (max-width: 1570px) {
              .welcome-text{
                margin-top: 0;
                margin-right: 30px;
                margin-bottom: 60px;
              }
            }

            @media screen and (min-width: 1570px) {
              .welcome-text{
                margin: 30px;
              }


            }

            .custom-nav {
                background: #1D3062;
                margin: 0px;
            }





            .right-text{
                margin-right: 0px;
                padding-right: 0px;
                text-align: right;
                color: #ffffff;
                position: relative;
            }

            .right-nav{
                position: absolute;
                bottom: 0;
                right: 0;
            }

            .active{
                background-color: #0E1F4D;
            }

            .nav-link{
                color: #ffffff;
                width: 100px;
                text-align: center;
            }

            a:hover{
                color: #8ACCE9;
            }

            .dropdown-menu{
                background-color: #1D3062;
            }

            .dropdown-item{
                color: #fff;
            }

            .dropdown-menu a:hover{
                background-color: #0E1F4D;
                color: #fff;
            }

            .clearfix::after,
            .calendar ol::after {
            content: ".";
            display: block;
            height: 0;
            clear: both;
            visibility: hidden;
            }

            /* ================
            Calendar Styling */
            .calendar {
            border-radius: 10px;
            }

            .month {
            font-size: 2rem;
            }

            @media (min-width: 992px) {
            .month {
                font-size: 3.5rem;
            }
            }

            .calendar ol li {
            float: left;
            width: 14.28571%;
            }

            .calendar .day-names {
            border-bottom: 1px solid #eee;
            }

            .calendar .day-names li {
            text-transform: uppercase;
            margin-bottom: 0.5rem;
            }

            .calendar .days li {
            border-bottom: 1px solid #eee;
            min-height: 6.5rem;
            padding: 0px 8px;
            }

            .calendar .days li.current {
                background-color: #4A5D8D;
                color: #ffffff;
                border-bottom: 1px solid #eee;
                min-height: 6.5rem;
                border-radius: 10px;
                padding: 0px 8px;
            }
            .calendar .days li .date {
            margin: 0.5rem 0;
            }

            .calendar .days li .date.selected {
            background-color: #1D3062;
            width: fit-content;
            padding: 1px 8px;
            border-radius: 30px;
            color: #fff;
            }


            .calendar .days li .event {
            font-size: 0.75rem;
            padding: 0.4rem;
            color: white;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            border-radius: 5px;
            margin: 2px;
            width: 15px;
            height: 15px;
            }

            .calendar .days li .event.e1{
                background-color: #B88778;
            }

            .calendar .days li .event.e2{
                background-color: #B9C687;
            }

            .calendar .days li .event.e3{
                background-color: #8ACCE9;
            }

            .calendar .days li .event.e4{
                background-color: #C69FC7;
            }

            .calendar .days li .event.e5{
                background-color: #E46B6B;
            }

            .calendar .days li .event.e6{
                background-color: #FFD585;
            }

            .calendar .days li .event.e7{
                background-color: #BAF4B9;
            }

            .calendar .days li .event.e8{
                background-color: #84A7CF;
            }

            .calendar .days li .event.e9{
                background-color: #9E62B2;
            }

            .calendar .days li .event.e10{
                background-color: #ED78A9;
            }

            .calendar .days li .event.e11{
                background-color: #B88778;
            }

            .calendar .days li .event.e12{
                background-color: #B9C687;
            }

            .calendar .days li .event.e13{
                background-color: #8ACCE9;
            }

            .calendar .days li .event.e14{
                background-color: #C69FC7;
            }

            .calendar .days li .event.e15{
                background-color: #E46B6B;
            }

            .calendar .days li .event.e16{
                background-color: #FFD585;
            }

            .calendar .days li .event.e17{
                background-color: #BAF4B9;
            }

            .calendar .days li .event.e18{
                background-color: #84A7CF;
            }

            .calendar .days li .event.e19{
                background-color: #9E62B2;
            }

            .calendar .days li .event.e20{
                background-color: #ED78A9;
            }

            .calendar .days li .event.span-2 {
            width: 200%;
            }

            .calendar .days li .event.begin {
            border-radius: 1rem 0 0 1rem;
            }

            .calendar .days li .event.end {
            border-radius: 0 1rem 1rem 0;
            }

            .calendar .days li .event.clear {
            background: none;
            }

            .calendar .days li:nth-child(n+29) {
            border-bottom: none;
            }

            .calendar .days li.outside .date {
            color: #fff;
            }

            .day-event1{
                background-color: #B88778;
            }
            .day-event2{
                background-color: #B9C687;
            }
            .day-event3{
                background-color: #8ACCE9;
            }
            .day-event4{
                background-color: #C69FC7;
            }
            .day-event5{
                background-color: #E46B6B;
            }

            .day-event6{
                background-color: #FFD585;
            }
            .day-event7{
                background-color: #BAF4B9;
            }
            .day-event8{
                background-color: #8ACCE9;
            }
            .day-event9{
                background-color: #9E62B2;
            }
            .day-event10{
                background-color: #ED78A9;
            }

            .day-event11{
                background-color: #B88778;
            }
            .day-event12{
                background-color: #B9C687;
            }
            .day-event13{
                background-color: #8ACCE9;
            }
            .day-event14{
                background-color: #C69FC7;
            }
            .day-event15{
                background-color: #E46B6B;
            }

            .day-event16{
                background-color: #FFD585;
            }
            .day-event17{
                background-color: #BAF4B9;
            }
            .day-event18{
                background-color: #8ACCE9;
            }
            .day-event19{
                background-color: #9E62B2;
            }
            .day-event20{
                background-color: #ED78A9;
            }

            td div{
                width: 100%;
                height: 50px;
                border-radius: 20px;
            }
            th {
                width: 144px;
                height: 50px;
                text-align: center;
            }
        </style>

    </head>
    @php

    @endphp


    <body >
        <div class="custom-nav row">
            <div>
                <img class="logo" src="http://sapsp.uum.edu.my/images/Logo-SAPSP_DarkBG.png" alt="profile Pic" height="140px">
            </div>
            <div class="container right-text">
                <h2 class="welcome-text">SAPSP BOOKING SYSTEM</h2>
                <nav class="nav float-right right-nav">
                    <a class="nav-link active" href="#">Home</a>
                    <a class="nav-link " href="book">Book</a>
                    <a class="nav-link" href="adminRequest">Request</a>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Admin</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="adminLog">Logs</a>
                            @if (Auth::user()->role === 0)
                                <a class="dropdown-item" href="adminLab">Labs</a>
                            @endif
                            <a class="dropdown-item" href="userList">Users</a>
                            <a class="dropdown-item" href="userSetting">Setting</a>
                            <a class="dropdown-item"href="adminLogout">Logout</a>
                        </div>
                    </li>

                </nav>
            </div>

        </div>
        <div>
            @php
                $month = Carbon\Carbon::now()->addHours(8)->month;
                $year = Carbon\Carbon::now()->addHours(8)->year;
                $today = Carbon\Carbon::now()->addHours(8)->day;
                $todayString = $today.'-'.$month.'-'.$year;
                $t = Carbon\Carbon::now()->addHours(8)->daysInMonth;
                $date = $year.'-'.$month.'-1';
                $day = Carbon\Carbon::parse($date)->format('l');
                $current = 'current';

                if ($day === 'Sunday') {
                    $start = 1;
                }
                elseif ($day === 'Monday') {
                    $start = 0;
                }
                elseif ($day === 'Tuesday') {
                    $start = -1;
                }
                elseif ($day === 'Wednesday') {
                    $start = -2;
                }
                elseif ($day === 'Thursday') {
                    $start = -3;
                }
                elseif ($day === 'Friday') {
                    $start = -4;
                }
                elseif ($day === 'Saturday') {
                    $start = -5;
                }
            @endphp


        </div>

        <div class="row" style="margin-left: 0; margin-right:0">
          <div class="col">
            <div class="calendar bg-white p-5" style="min-width: 400px">
              <div class="d-flex align-items-center"><i class="fa fa-calendar fa-3x mr-3"></i>
              <h2 id="selectedMonth" class="month font-weight-bold mb-0 text-uppercase">{{Carbon\Carbon::parse($todayString)->format('F')}} {{$year}}</h2>
              </div>
              <div class="">
                <button id="prevButton" class="btn" style="margin:30px 0px; background-color: #4A5D8D; color: #fff"><</button>
                <button id="nextButton" class="btn float-right" style="margin:30px 0px; background-color: #4A5D8D; color: #fff">></button>
              </div>
              <ol class="day-names list-unstyled">
                <li class="font-weight-bold text-uppercase">Sun</li>
                <li class="font-weight-bold text-uppercase">Mon</li>
                <li class="font-weight-bold text-uppercase">Tue</li>
                <li class="font-weight-bold text-uppercase">Wed</li>
                <li class="font-weight-bold text-uppercase">Thu</li>
                <li class="font-weight-bold text-uppercase">Fri</li>
                <li class="font-weight-bold text-uppercase">Sat</li>
              </ol>

              <ol class="days list-unstyled" id="tesuto">


                @for($loop = $start; $loop <= $t; $loop++)
                    @if ($loop<1)
                        <li class="outside">
                        <div class="date" id="day{{$loop}}">{{$loop}}</div>
                        </li>
                    @else
                        @if ($loop.'-'.$month.'-'.$year === $todayString)
                            <li class="{{$current}}">
                                <div class="date" id="day{{$loop}}">{{$loop}}</div>
                                <div class="row" style="margin-left: 5px; margin-right: 5px" id="event{{$loop}}">

                                </div>
                            </li>
                        @else
                            <li >
                                <div class="date" id="day{{$loop}}">{{$loop}}</div>
                                <div class="row" style="margin-left: 5px; margin-right: 5px" id="event{{$loop}}">

                                </div>
                            </li>
                        @endif

                    @endif
                @endfor

              </ol>
            </div>
          </div>

          <div class="col" style="background-color:#4A5D8D">

    <h3 id="table-date" class="font-weight-bold mb-0 text-uppercase" style="margin: 95px 0px 0px 20px; color: #fff">{{ $today }} {{Carbon\Carbon::parse($todayString)->format('F')}} {{$year}}</h3>

            <table id="table" class="table table-striped table-dark" style="border-radius: 20px; margin-top: 20px">
                <thead>
                    <th></th>
                    @foreach ($labs as $lab)
                        <th>{{$lab->name}}</th>
                    @endforeach
                </thead>
                <tbody>
                    @for($tr = 8; $tr < 18; $tr++)
                        <tr>
                            <th>{{$tr.':00 - '. ($tr+1).':00'}}</th>
                            @for($l = 0; $l < count($labIdArr); $l++)
                                <td>
                                    <div id="lab{{$labIdArr[$l]}}time{{$tr}}">

                                    </div>
                                </td>
                            @endfor
                        </tr>
                    @endfor
                </tbody>
            </table>
          </div>
        </div>


    </body>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
    <script>
        var month = {{$month}};
        var year = {{$year}};
        var today = {{$today}};
        var day = "{{$day}}";
        var dayName = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
        var monthName = ["Bang2","January","February","March","April","May","June","July","August","September","October","November","December"];

        function myFunction() {
            var todayString = "{{$todayString}}";
            var t = {{$t}};
            var date = {{$date}};
            var start = 0;
            if (day == 'Sunday') {
                start = 1;
            }
            else if (day == 'Monday') {
                start = 0;
            }
            else if (day == 'Tuesday') {
                start = -1;
            }
            else if (day == 'Wednesday') {
                start = -2;
            }
            else if (day == 'Thursday') {
                start = -3;
            }
            else if (day == 'Friday') {
                start = -4;
            }
            else if (day == 'Saturday') {
                start = -5;
            }


            var tesuto = document.getElementById("tesuto");

            while (tesuto.hasChildNodes()) {
                tesuto.removeChild(tesuto.firstChild);
            }

            for(loop = start; loop <= t; loop++){
                var tata = document.createTextNode(loop);
                var dateDiv = document.createElement('div');
                var eventContainerDiv = document.createElement('div');
                dateDiv.setAttribute("class","date");
                dateDiv.setAttribute("id","day"+loop);
                dateDiv.appendChild(tata);
                eventContainerDiv.setAttribute("class","row");
                eventContainerDiv.setAttribute("style","margin-left: 5px; margin-right: 5px");
                eventContainerDiv.setAttribute("id","event"+loop);

                if (loop<1){
                    var li = document.createElement('li');
                    li.setAttribute("class","outside");
                    li.appendChild(dateDiv);
                    document.getElementById("tesuto").appendChild(li);
                }
                else{
                    if (loop+'-'+month+'-'+year == todayString){
                        var li = document.createElement('li');
                        li.setAttribute("class","current");
                        li.appendChild(dateDiv);
                        li.appendChild(eventContainerDiv);
                        document.getElementById("tesuto").appendChild(li);
                    }
                    else{
                        var li = document.createElement('li');
                        li.setAttribute("class","");
                        li.appendChild(dateDiv);
                        li.appendChild(eventContainerDiv);
                        document.getElementById("tesuto").appendChild(li);
                    }
                }
            }

        }

        let prevButton = document.getElementById('prevButton');

        prevButton.onclick = function prevMonth(){
            if( month == 1){
                year = year-1;
                month = 12;
                monthFirstDay = new Date(monthName[month]+" 1, "+year+" 11:10:00");

                day =  dayName[monthFirstDay.getDay()];


                console.log(day+" "+monthName[month]);
            }
            else{
                month = month-1;
                monthFirstDay = new Date(monthName[month]+" 1, "+year+" 11:10:00");

                day =  dayName[monthFirstDay.getDay()];
                console.log(day+" "+monthName[month]);
            }
            document.getElementById("selectedMonth").innerHTML = monthName[month]+" "+year;
            myFunction();
            selectedMonth();
        }

        let nextButton = document.getElementById('nextButton');

        nextButton.onclick = function prevMonth(){
            if( month == 12){
                year = year+1;
                month = 1;
                monthFirstDay = new Date(monthName[month]+" 1, "+year+" 11:10:00");

                day =  dayName[monthFirstDay.getDay()];
                console.log(day+" "+monthName[month]);
            }
            else{
                month = month+1;
                monthFirstDay = new Date(monthName[month]+" 1, "+year+" 11:10:00");

                day =  dayName[monthFirstDay.getDay()];
                console.log(day+" "+monthName[month]);
            }
            document.getElementById("selectedMonth").innerHTML = monthName[month]+" "+year;
            myFunction();
            selectedMonth();
        }
        var monthString = month;
        if(month<10){
            monthString = "0"+month;
        }
        $(document).ready(selectedMonth());
        $('#table').ready(selectedDate(year+"-"+monthString+"-"+today));

        function selectedMonth(){
            $.ajax({
                url: "{{url('/selectedMonth')}}",
                method: "GET",
                data: {year: year, month: month},
                success:function(data){
                    var bookList = jQuery.parseJSON(data);

                    for(i = 0; i< Object.keys(bookList).length;i++){

                        if(i == 0){
                            var eventDiv = document.createElement('div');
                            eventDiv.setAttribute("class","event e"+bookList[i].lab_id);

                            $('#event'+parseInt(bookList[i].book_date.substring(8,10))).append(eventDiv);

                        }
                        else{
                            if(!(bookList[i].book_date == bookList[i-1].book_date && bookList[i].lab_id == bookList[i-1].lab_id)){
                                var eventDiv = document.createElement('div');
                                eventDiv.setAttribute("class","event e"+bookList[i].lab_id);

                                $('#event'+parseInt(bookList[i].book_date.substring(8,10))).append(eventDiv);
                            }
                        }

                    }
                }
            })
        }

        function selectedDate(passDate){
            $.ajax({
                url: "/selectedDateEvent",
                method: "GET",
                data: {date: passDate},
                success:function(data){
                    var arr = {{ json_encode($labIdArr) }}
                    var events = jQuery.parseJSON(data);
                    for(i = 8; i < 18; i++){
                        for(j = 0; j < {{$count}}; j++){
                            document.getElementById("lab"+arr[j]+"time"+i).className = "";
                        }
                    }

                    for(i=0; i< Object.keys(events).length; i++){
                        $("#lab"+events[i].lab_id+"time"+parseInt(events[i].book_time)).addClass("day-event"+events[i].lab_id);
                    }
                }
            })
        }

    </script>
    <script>
        previousDay = "day0";

        function selectDate(e){
            e = e || window.event;
            return e.target || e.srcElement;
        }

        let ol = document.getElementById('tesuto');
        var t = {{$t}};


        ol.onclick = function clickedDay(event) {
            let target = selectDate(event);
            let lil = target.closest('li'); // get reference
            let nodes = Array.from( lil.closest('ol').children ); // get array
            let index = nodes.indexOf( lil );
            let day = index + 1 - (tesuto.childElementCount - t);
            tesuto = document.getElementById("tesuto");

            if(day > 0){
                if(previousDay != "day"+day){
                    var clickedDate = "";
                    document.getElementById("day"+day).className = "date selected";
                    document.getElementById(previousDay).className = "date";
                    document.getElementById("table-date").innerHTML = day+" "+monthName[month]+" "+year;

                    previousDay = "day"+day;
                    if(month < 10){
                        clickedDate = year+"-0"+month+"-"+day;

                        selectedDate(clickedDate);
                    }
                    else{
                        clickedDate = year+"-"+month+"-"+day;
                        selectedDate(clickedDate);
                    }
                }
            }


        }



    </script>

</html>
