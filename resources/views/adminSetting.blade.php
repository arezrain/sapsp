<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ Session::token() }}">

        <title>SAPSP Booking System</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />

        @php
            $superAdmin = false;
            if(Auth::user()->role == 0){
                $superAdmin = true;
            }
        @endphp

        <style>

            @media screen and (min-width: 870px) {
            }

            @media screen and (max-width: 870px) {
            }

            @media screen and (max-width: 1570px) {
              .welcome-text{
                margin-top: 0;
                margin-right: 30px;
                margin-bottom: 60px;
              }
            }

            @media screen and (min-width: 1570px) {
              .welcome-text{
                margin: 30px;
              }
            }

            .custom-nav {
                background: #1D3062;
                margin: 0px;
            }

            .logo {
                margin: 30px 26px 40px 20px;
            }



            .right-text{
                margin-right: 0px;
                padding-right: 0px;
                text-align: right;
                color: #ffffff;
                position: relative;
            }

            .right-nav{
                position: absolute;
                bottom: 0;
                right: 0;
            }

            .active{
                background-color: #0E1F4D;
            }

            .nav-link{
                color: #ffffff;
                width: 100px;
                text-align: center;
            }

            a:hover{
                color: #8ACCE9;
            }

            .dropdown-menu{
                background-color: #1D3062;
            }

            .dropdown-item{
                color: #fff;
            }

            .dropdown-menu a:hover{
                background-color: #0c193d;
                color: #fff;
            }

            .dropdown-item.active{
                background-color: #0E1F4D;
                color: #fff;
            }

            table {
                text-align: center;
            }

        </style>

    </head>
    @php

    @endphp


    <body >

        <div class="custom-nav row">
            <div>
                <img class="logo" src="http://sapsp.uum.edu.my/images/Logo-SAPSP_DarkBG.png" alt="profile Pic" height="140px">
            </div>
            <div class="container right-text">
                <h2 class="welcome-text">SAPSP BOOKING SYSTEM</h2>
                <nav class="nav float-right right-nav">
                    <a class="nav-link" href="/sapsp-admin">Home</a>
                    <a class="nav-link" href="book">Book</a>
                    <a class="nav-link" href="adminRequest">Request</a>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle active" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Admin</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="adminLog">Logs</a>
                            <a class="dropdown-item" href="adminLab">Labs</a>
                            <a class="dropdown-item" href="userList">Users</a>
                            <a class="dropdown-item active" href="#">Setting</a>
                            <a class="dropdown-item"href="adminLogout">Logout</a>
                        </div>
                    </li>
                </nav>
            </div>
        </div>

        <div class="container" style="background-color: whitesmoke; border-radius:20px; max-width:650px; padding:30px; margin-top:30px">
            <form method="POST" action="changeDetail">
                {{ csrf_field() }}
                <h3>Edit User Details</h3>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" placeholder="Enter full name" name="name">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                </div>
                <div class="form-group">
                    <label for="contact">Phone no.</label>
                    <input type="tel" class="form-control" id="contact" placeholder="Enter contact no" name="contact">
                  </div>
                <button type="submit" class="btn btn-primary">Save</button>
              </form>
        </div>

        <div class="container" style="background-color: whitesmoke; border-radius:20px; max-width:650px; padding:30px; margin-top:30px">
            <form method="POST" action="changePassword">
                {{ csrf_field() }}
                <h3>Change Password</h3>
                <div class="form-group">
                    <label for="oldPassword">Old Password</label>
                    <input type="password" class="form-control" id="oldPassword" placeholder="Enter old password" name="oldPassword">
                </div>
                <div class="form-group">
                    <label for="newPassword">New Password</label>
                    <input type="password" class="form-control" id="newPassword" placeholder="Enter new password" name="newPassword">
                </div>
                <div class="form-group">
                    <label for="rePassword">Retype New Password</label>
                    <input type="password" class="form-control" id="rePassword" placeholder="Retype new password" name="rePassword">
                  </div>
                <button type="submit" class="btn btn-primary">Save</button>
              </form>
        </div>

    </body>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
    <script>
        $("#name").val('{{$user->name}}');
        $("#email").val('{{$user->email}}');
        $("#contact").val('{{$user->contact}}');
    </script>
    @if (Session::has('alert'))
        <script>
            alert("{{ session()->get('alert') }}");
        </script>
    @endif
</html>
