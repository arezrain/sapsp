<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ Session::token() }}">

        <title>Home</title>
        @if (!Auth::check())
            <script>window.location = "/";</script>
        @endif

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <style>

            @media screen and (min-width: 870px) {
                .booked-list{
                    /* width: 727px; */
                    height: 136px;

                    background: #1D3062;
                    border: 3px solid #1D3062;
                    box-sizing: border-box;
                    border-radius: 15px;
                    padding: 15px;
                    margin: 20px;
                    color: white;
                }

                .booked-list button{
                    margin-top: -40px;
                    background-color: #E16E6E;
                    border-color: #c04949;
                }

                .history-list button{
                    margin-top: -30px;
                }

                .history-list{
                    /* width: 727px; */
                    height: 136px;

                    background: #4A5D8D;
                    border: 3px solid #1D3062;
                    box-sizing: border-box;
                    border-radius: 15px;
                    padding: 15px;
                    margin: 20px;
                    color: white;
                }
            }

            @media screen and (max-width: 870px) {
                .booked-list{
                    /* width: 727px; */
                    height: 136px;

                    background: #1D3062;
                    border: 3px solid #1D3062;
                    box-sizing: border-box;
                    border-radius: 15px;
                    padding: 10px;
                    margin: 10px;
                    color: white;
                }

                .booked-list button{
                    margin-top: -24px;
                    margin-right: -10px;
                    background-color: #E16E6E;
                    border-color: #c04949;
                    font-size: 12px;
                    border-bottom-right-radius: 13px;
                }

                .history-list{
                    /* width: 727px; */
                    height: 136px;

                    background: #4A5D8D;
                    border: 3px solid #1D3062;
                    box-sizing: border-box;
                    border-radius: 15px;
                    padding: 10px;
                    margin: 10px;
                    color: white;
                }

                .history-list button{
                    margin-top: -2px;
                    margin-right: -10px;
                    font-size: 12px;
                    border-bottom-right-radius: 13px;
                }
            }





            .booked-list button:hover{
                background-color: #b63232;
                border-color: #721616
            }

            .book-title{
                font-family: Roboto;
                font-style: normal;
                color: #24233d;
                font-size: 20px;
                margin-top: 30px;
                margin-bottom: 20px;
            }



            .history-title{
                font-family: Roboto;
                font-style: normal;
                color: #24233d;
                font-size: 20px;
                margin-top: -45px;
            }

            .history{
                margin-top: 100px;
            }

            .pending{
                background-color: dimgrey;
                width: 80px;
                text-align: center;
                font-size: 13px;
                padding: 2px;
                border-radius: 9px;
            }

            .approved{
                background-color: seagreen;
                width: 80px;
                text-align: center;
                font-size: 13px;
                padding: 2px;
                border-radius: 9px;
            }

            .rejected{
                background-color: darkred;
                width: 80px;
                text-align: center;
                font-size: 13px;
                padding: 2px;
                border-radius: 9px;
            }

        </style>

    </head>


    <body style="background-color: rgb(168, 172, 207)">

            <nav class="navbar navbar-expand-sm navbar-dark" style="background-color: #4A5D8D">
                <a class="navbar-brand" href="#"><img class="logo" src="http://sapsp.uum.edu.my/images/Logo-SAPSP_DarkBG.png" alt="profile Pic" height="30px"></a>
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="collapsibleNavId">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0 float-right">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('/book')}}">Book</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="userSetting">Setting</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('/logout')}}">Logout</a>
                        </li>
                    </ul>
                </div>
            </nav>


            <div class="container">
                <div class="booking-container justify-content-center">
                    <h2 class="book-title">Booked lab</h2>
                    <div>
                        @php
                            $month = Carbon\Carbon::now()->month;
                            $year = Carbon\Carbon::now()->year;
                            $today = Carbon\Carbon::now()->day;
                        @endphp
                        @foreach ($bookings as $booking)
                            @if ((int)substr($booking->book_date,0,4) >= $year)
                                @if ((int)substr($booking->book_date,5,7) >= $month)
                                    @if ((int)substr($booking->book_date,8,10) >= $today)
                                        <div class="booked-list">
                                            <h4>Lab : {{$booking->name}}</h4>
                                            <h6>Description : {{$booking->description}}</h6>
                                            <h6>Date : {{$booking->book_date}}      Time : {{substr($booking->book_time,0,5)}}</h6>
                                            @if ($booking->status === 'approved')
                                                <div class="approved">approved</div>
                                                <button type="button" onclick="testid({{$booking->id}})" class="btn btn-primary float-right" data-toggle="modal" data-target="#cancelBookingModal">cancel booking</button>

                                            @elseif ($booking->status === 'pending')
                                                <div class="pending">pending</div>
                                                <button type="button" onclick="testid({{$booking->id}})" class="btn btn-primary float-right" data-toggle="modal" data-target="#cancelBookingModal">cancel booking</button>

                                            @elseif ($booking->status === 'rejected')
                                                <div class="rejected">rejected</div>
                                                <button type="button" onclick="testid({{$booking->id}})" class="btn btn-primary float-right" data-toggle="modal" data-target="#cancelBookingModal">clear booking</button>

                                            @endif
                                        </div>
                                    @endif
                                @endif
                            @endif

                        @endforeach
                    </div>
                </div>
            </div>


            <div class="modal fade" id="cancelBookingModal" tabindex="-1" role="dialog" aria-labelledby="cancelBookingModalTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="cancelBookingModalLongTitle">Alert</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Are you sure want to cancel booking this lab?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary" onclick="cancelBooking()">Proceed</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="removeHistoryModal" tabindex="-1" role="dialog" aria-labelledby="removeHistoryModalTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="removeHistoryModalLongTitle">Alert</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Are you sure want to remove this history?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary" onclick="cancelBooking()">Proceed</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="clearHistoryModal" tabindex="-1" role="dialog" aria-labelledby="clearHistoryModalTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="clearHistoryModalLongTitle">Alert</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Are you sure want to clear the history?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary" onclick="clearHistory()">Proceed</button>
                        </div>
                    </div>
                </div>
            </div>

    </body>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        var getId = 0;

        function testid(id){
            getId = id;
        }

        function cancelBooking(){
            $.post("/cancelBooking",{
                    '_token': $('meta[name=csrf-token]').attr('content'),
                    id: getId,
                },function(data,status){
                    if(status == 'success'){
                        location.reload();
                    }
                }
            )
        }


    </script>
    <script>
        function clearHistory(){
            $.ajax({
                        url: "{{url('/clearHistory')}}",
                        method: "GET",
                        data: {date: 'now()'},
                        success:function(data){
                            location.reload();
                        }
                    })
        }
    </script>
</html>
