<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ Session::token() }}">

        <title>Home</title>
        @if (!Auth::check())
            <script>window.location = "/";</script>
        @endif

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <style>

            @media screen and (min-width: 870px) {
                .booked-list{
                    /* width: 727px; */
                    height: 136px;

                    background: #1D3062;
                    border: 3px solid #1D3062;
                    box-sizing: border-box;
                    border-radius: 15px;
                    padding: 15px;
                    margin: 20px;
                    color: white;
                }

                .booked-list button{
                    margin-top: -30px;
                    background-color: #E16E6E;
                    border-color: #c04949;
                }

                .history-list button{
                    margin-top: -30px;
                }

                .history-list{
                    /* width: 727px; */
                    height: 136px;

                    background: #4A5D8D;
                    border: 3px solid #1D3062;
                    box-sizing: border-box;
                    border-radius: 15px;
                    padding: 15px;
                    margin: 20px;
                    color: white;
                }
            }

            @media screen and (max-width: 870px) {
                .booked-list{
                    /* width: 727px; */
                    height: 136px;

                    background: #1D3062;
                    border: 3px solid #1D3062;
                    box-sizing: border-box;
                    border-radius: 15px;
                    padding: 10px;
                    margin: 10px;
                    color: white;
                }

                .booked-list button{
                    margin-top: -2px;
                    margin-right: -10px;
                    background-color: #E16E6E;
                    border-color: #c04949;
                    font-size: 12px;
                    border-bottom-right-radius: 13px;
                }

                .history-list{
                    /* width: 727px; */
                    height: 136px;

                    background: #4A5D8D;
                    border: 3px solid #1D3062;
                    box-sizing: border-box;
                    border-radius: 15px;
                    padding: 10px;
                    margin: 10px;
                    color: white;
                }

                .history-list button{
                    margin-top: -2px;
                    margin-right: -10px;
                    font-size: 12px;
                    border-bottom-right-radius: 13px;
                }
            }





            .booked-list button:hover{
                background-color: #b63232;
                border-color: #721616
            }

            .book-title{
                font-family: Roboto;
                font-style: normal;
                color: #24233d;
                font-size: 20px;
                margin-top: 30px;
                margin-bottom: 20px;
            }



            .history-title{
                font-family: Roboto;
                font-style: normal;
                color: #24233d;
                font-size: 20px;
                margin-top: -45px;
            }

            .history{
                margin-top: 100px;
            }

        </style>

    </head>


    <body style="background-color: rgb(168, 172, 207)">

            <nav class="navbar navbar-expand-sm navbar-dark" style="background-color: #4A5D8D">
                <a class="navbar-brand" href="#"><img class="logo" src="http://sapsp.uum.edu.my/images/Logo-SAPSP_DarkBG.png" alt="profile Pic" height="30px"></a>
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="collapsibleNavId">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0 float-right">
                        <li class="nav-item">
                            <a class="nav-link" href="home">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('/book')}}">Book</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Setting</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('/logout')}}">Logout</a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="container" style="background-color: white; border-radius:20px; max-width:650px; padding:30px; margin-top:30px">
                <form method="POST" action="changeDetail">
                    {{ csrf_field() }}
                    <h3>Edit User Details</h3>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Enter full name" name="name">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="contact">Phone no.</label>
                        <input type="tel" class="form-control" id="contact" placeholder="Enter contact no" name="contact">
                      </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                  </form>
            </div>

            <div class="container" style="background-color: white; border-radius:20px; max-width:650px; padding:30px; margin-top:30px">
                <form method="POST" action="changePassword">
                    {{ csrf_field() }}
                    <h3>Change Password</h3>
                    <div class="form-group">
                        <label for="oldPassword">Old Password</label>
                        <input type="password" class="form-control" id="oldPassword" placeholder="Enter old password" name="oldPassword">
                    </div>
                    <div class="form-group">
                        <label for="newPassword">New Password</label>
                        <input type="password" class="form-control" id="newPassword" placeholder="Enter new password" name="newPassword">
                    </div>
                    <div class="form-group">
                        <label for="rePassword">Retype New Password</label>
                        <input type="password" class="form-control" id="rePassword" placeholder="Retype new password" name="rePassword">
                      </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                  </form>
            </div>

    </body>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $("#name").val('{{$user->name}}');
        $("#email").val('{{$user->email}}');
        $("#contact").val('{{$user->contact}}');
    </script>
    @if (Session::has('alert'))
        <script>
            alert("{{ session()->get('alert') }}");
        </script>
    @endif
</html>
