<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ Session::token() }}">

        <title>SAPSP Booking System</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />


        <style>

            @media screen and (min-width: 870px) {
            }

            @media screen and (max-width: 870px) {
            }

            @media screen and (max-width: 1570px) {
              .welcome-text{
                margin-top: 0;
                margin-right: 30px;
                margin-bottom: 60px;
              }
            }

            @media screen and (min-width: 1570px) {
              .welcome-text{
                margin: 30px;
              }
            }

            .custom-nav {
                background: #1D3062;
                margin: 0px;
            }

            .logo {
                margin: 30px 26px 40px 20px;
            }



            .right-text{
                margin-right: 0px;
                padding-right: 0px;
                text-align: right;
                color: #ffffff;
                position: relative;
            }

            .right-nav{
                position: absolute;
                bottom: 0;
                right: 0;
            }

            .active{
                background-color: #0E1F4D;
            }

            .nav-link{
                color: #ffffff;
                width: 100px;
                text-align: center;
            }

            a:hover{
                color: #8ACCE9;
            }

            .dropdown-menu{
                background-color: #1D3062;
            }

            .dropdown-item{
                color: #fff;
            }

            .dropdown-menu a:hover{
                background-color: #0c193d;
                color: #fff;
            }

            .dropdown-item.active{
                background-color: #0E1F4D;
                color: #fff;
            }

            .table td{
                border-left: 1px solid #aaaaaa;
                border-right: 1px solid #aaaaaa;
                border-bottom: 1px solid #aaaaaa;
            }

            .table th{
                border-left: 1px solid #aaaaaa;
                border-right: 1px solid #aaaaaa;
            }

        </style>

    </head>
    @php

    @endphp


    <body >
        <div class="custom-nav row">
            <div>
                <img class="logo" src="http://sapsp.uum.edu.my/images/Logo-SAPSP_DarkBG.png" alt="profile Pic" height="140px">
            </div>
            <div class="container right-text">
                <h2 class="welcome-text">SAPSP BOOKING SYSTEM</h2>
                <nav class="nav float-right right-nav">
                    <a class="nav-link" href="/sapsp-admin">Home</a>
                    <a class="nav-link" href="book">Book</a>
                    <a class="nav-link active" href="#">Request</a>
                    {{-- <a class="nav-link" href="">Book</a> --}}
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle " data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Admin</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="adminLog">Logs</a>
                            @if (Auth::user()->role === 0)
                                <a class="dropdown-item" href="adminLab">Labs</a>
                            @endif                            
                            <a class="dropdown-item" href="userList">Users</a>
                            <a class="dropdown-item" href="userSetting">Setting</a>
                            <a class="dropdown-item"href="adminLogout">Logout</a>
                        </div>
                    </li>
                </nav>
            </div>
        </div>

        


        <div class="container">
            <table class="table" style="margin:30px 15px 50px 15px">
                <thead>
                    <th>Lab</th>
                    <th>Book Date</th>
                    <th>Book Time</th>
                    <th>Purpose</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Action</th>
                </thead>
                <tbody id="list">
                </tbody>

            </table>
        </div>


        <div class="modal fade" id="acceptModal" tabindex="-1" role="dialog" aria-labelledby="acceptModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="acceptModalLabel">Confirmation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="/editLab">
                        <div class="modal-body">
                            Are you sure want to accept this user request?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary" onclick="accept()" data-dismiss="modal">Confirm</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="rejectModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="rejectModalLabel">Confirmation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="/editLab">
                        <div class="modal-body">
                            Are you sure want to reject this user request?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary" onclick="reject()" data-dismiss="modal">Confirm</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </body>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
    <script>

        var id = 0;

        function setId(getid){
            id = getid;
            alert(id);
        }

        $(document).ready(fetcRequest());

        function fetcRequest(){
            $('#list').empty();
            $.get('requestList',{
                data : 'true',
            },function(data, status){
                if(status == 'success'){
                    for( var i = 0; i < Object.keys(data).length; i++){
                        $('#list').append('<tr><td>'+data[i].lab_name+
                        '</td><td>'+data[i].book_date+
                        '</td><td>'+data[i].book_time+
                        '</td><td>'+data[i].desrciption+
                        '</td><td>'+data[i].user_name+
                        '</td><td>'+data[i].contact+
                        '</td><td><button type="button" class="btn btn-primary" style="margin-right: 10px" data-toggle="modal" data-target="#acceptModal" onclick="setId('+data[i].id+')">approve</button>'+
                        '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#rejectModal" onclick="setId('+data[i].id+')">reject</button></td></tr>')
                    }
                }
            })
        }

        function accept(){
            $.post("acceptRequest",{
                                '_token': $('meta[name=csrf-token]').attr('content'),
                                id: id,
                            },function(data,status){
                                if(status === 'success'){
                                    fetcRequest();
                                }
                            });
        }   

        function reject(){
            $.post("rejectRequest",{
                                '_token': $('meta[name=csrf-token]').attr('content'),
                                id: id,
                            },function(data,status){
                                if(status === 'success'){
                                    fetcRequest();
                                }
                            });
        }
        


    </script>
</html>
