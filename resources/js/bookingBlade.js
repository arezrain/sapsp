var before = true;
        function getToday(){
            var today = new Date();
            var dd = String(today.getDate()).padStart(2,'0');
            var mm = String(today.getMonth()+1).padStart(2,'0');
            var yyyy = today.getFullYear();

            today = mm+'/'+dd+'/'+yyyy;
            return today;
        }

        function dayDifferent(current, selected){
            dt1 = new Date(current);
            dt2 = new Date(selected);
            return Math.floor((Date.UTC(dt2.getFullYear(),dt2.getMonth(),dt2.getDate()) - 
            Date.UTC(dt1.getFullYear(),dt1.getMonth(),dt1.getDate()) ) /(1000 * 60 * 60 * 24 ));
        }

        function selectedLab(labName,labId){
            document.getElementById("labDropdown").innerHTML = labName;
            // e.preventDefault();
            var getLabNAme = document.getElementById('labDropdown').innerHTML;
            // alert("works "+getLabNAme);
            $.ajax({
                url: "{{url('/selected')}}",
                method: "GET",
                data: {date: $("#date").val(), lab: labName},
                success:function(data){
                    var time = JSON.parse(data);
                    var timeSize = Object.keys(time).length;
                    for(j = 8 ; j < 18; j++){
                        if(j<10){
                            $('#0'+j).removeClass('disabled');
                            $('#0'+j).removeClass('selected');
                        }else{
                            $('#'+j).removeClass('disabled');
                            $('#'+j).removeClass('selected');
                        }
                    }
                    for(i = 0; i < timeSize; i++){
                        for(j = 8 ; j < 18; j++){
                            if(j == parseInt(time[i].book_time.substring(0,2))){
                                $('#'+time[i].book_time.substring(0,2)).toggleClass('disabled');
                            }
                            // else{
                            //     $('#'+time[i].book_time.substring(0,2)).removeClass('disabled');
                            // }
                        }
                    }
                }
            })
        }

        function selectTime(timeId){
            if(!$('#'+timeId).hasClass('disabled')){
                if($('#'+timeId).hasClass('selected')){
                $('#'+timeId).removeClass('selected');
                }
                else{
                    $('#'+timeId).addClass('selected');
                }
            }

        }

        function submitBook(){
            var getLabNAme = document.getElementById('labDropdown').innerHTML;
            
            if($('#date') != 'mm/dd/yyyy' && getLabNAme != 'Choose Lab' && $("#description").val() != "" && before){
                for( i = 8 ; i < 18 ; i++){
                    if(i<10){
                        if($('#0'+i).hasClass('selected')){
                            $.post("/booked",{
                                '_token': $('meta[name=csrf-token]').attr('content'),
                                user_id: "{{$userId}}",
                                user_name: "{{Auth::user()->name}}",
                                lab_name: getLabNAme,
                                book_date: $("#date").val(),
                                book_time: "0"+i+":00:00",
                                description: $('#description').val(),
                            },function(data,status){
                                if(status === 'success'){
                                    alert("Successfully book \nLab : "+getLabNAme+"\nDate : "+ $("#date").val()+ "\nTime : "+data+":00");
                                    $('#'+data).removeClass('selected');
                                    $('#'+data).addClass('disabled');

                                    $.post("send-mail",{
                                        '_token': $('meta[name=csrf-token]').attr('content'),
                                        id: "{{$userId}}",
                                        name: "{{Auth::user()->name}}",
                                        lab: getLabNAme,
                                        date: $("#date").val(),
                                        time: i+":00",
                                        description: $('#description').val(),
                                    },function(data,status){});
                                }
                            })
                        }
                    }else{
                        if($('#'+i).hasClass('selected')){
                            $.post("/booked",{
                                '_token': $('meta[name=csrf-token]').attr('content'),
                                user_id: "{{$userId}}",
                                user_name: "{{Auth::user()->name}}",
                                lab_name: getLabNAme,
                                book_date: $("#date").val(),
                                book_time: i+":00:00",
                                description: $('#description').val(),
                            },function(data,status){
                                if(status === 'success'){
                                    alert("Successfully book \nLab : "+getLabNAme+"\nDate : "+ $("#date").val()+ "\nTime : "+data+":00");
                                    $('#'+data).removeClass('selected');
                                    $('#'+data).addClass('disabled');

                                    $.post("send-mail",{
                                        '_token': $('meta[name=csrf-token]').attr('content'),
                                        id: "{{$userId}}",
                                        name: "{{Auth::user()->name}}",
                                        lab: getLabNAme,
                                        date: $("#date").val(),
                                        time: i+":00",
                                        description: $('#description').val(),
                                    },function(data,status){});
                                }

                            })
                        }
                    }
                }
            }
            else{
                alert("Please fill in all input field and make sure the booking is for 2 days after today");
            }

        }

        $('#date').change(function(e){
            e.preventDefault();
            var getLabNAme = document.getElementById('labDropdown').innerHTML;

            var gDay = $('#date').val().substring(8,10);
            var gMonth = $("#date").val().substring(5,7);
            var gYear = $("#date").val().substring(0,4);
            var gDate = gMonth+'/'+gDay+'/'+gYear;
            // alert($('#date').val());
            // alert(dayDifferent(getToday(),gDate));

            if( dayDifferent(getToday(),gDate) < 3){
                alert("Lab booking must be done for 2 days after today date");
                before = false;
            }
            else{
                before = true;
                if(getLabNAme != 'Choose Lab'){
                    $.ajax({
                        url: "{{url('/selected')}}",
                        method: "GET",
                        data: {date: $("#date").val(), lab: getLabNAme},
                        success:function(data){
                            var time = JSON.parse(data);
                            var timeSize = Object.keys(time).length;
                            for(j = 8 ; j < 18; j++){
                                if(j<10){
                                    $('#0'+j).removeClass('disabled');
                                    $('#0'+j).removeClass('selected');
                                }else{
                                    $('#'+j).removeClass('disabled');
                                    $('#'+j).removeClass('selected');
                                }
                            }
                            for(i = 0; i < timeSize; i++){
                                for(j = 8 ; j < 18; j++){
                                    if(j == parseInt(time[i].book_time.substring(0,2))){
                                        $('#'+time[i].book_time.substring(0,2)).toggleClass('disabled');
                                    }
                                    // else{
                                    //     $('#'+time[i].book_time.substring(0,2)).removeClass('disabled');
                                    // }
                                }
                                $('#teseract').append(time[i].book_time.substring(0,2));
                            }
                        }
                    })
                }
            }

            

        })