<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UserController@welcome');

Route::get('/home', 'BookingsController@userHome');

Route::get('/sapsp-admin', 'BookingsController@adminHome')->name('bookings.adminHome');

Route::resource('bookings', 'BookingsController');

Route::post('/register', 'UserController@registerStudent' );

Route::post('/login', 'UserController@loginStudent' );

Route::get('/logout', 'UserController@userLogout');

Route::get('/book', 'BookingsController@index');

Route::get('/selected','BookingsController@selectedDateLab');

Route::post('/booked','BookingsController@store');

Route::post('/cancelBooking','BookingsController@cancelBooking');

Route::get('/clearHistory','BookingsController@clearHistory');

Route::get('/selectedMonth','BookingsController@selectedMonth');

Route::get('/selectedDateEvent','BookingsController@selectedDateEvent');

Route::get('/adminLog', 'BookingsController@initialMonthlySummary');

Route::get('/adminLab', 'LabController@index');

Route::get('/adminRequest', 'BookingsController@requestPage');

Route::get('/userList', 'UserController@index');

Route::post('/editLab', 'LabController@editLab');

Route::post('/addLab', 'LabController@addLab');

Route::post('/resetUser', 'UserController@resetUser');

Route::post('/deleteUser', 'UserController@deleteUser');

Route::post('/addAdmin', 'UserController@addAdmin');

Route::post('send-mail','MailSend@mailsend');

Route::post('summaryDate','BookingsController@daySummary');

Route::get('admin-login', function () {
    return view('adminLogin');
});

Route::post('/adminSubmitLogin', 'UserController@adminLogin');

Route::get('adminLogout', 'UserController@adminLogout');

Route::get('super-admin-create', 'UserController@superAdminCreateIndex');

Route::post('superAdminRegister', 'UserController@superAdminRegister');

Route::post('admin/search', 'UserController@search');

Route::get('userSetting', 'UserController@userSetting');

Route::post('changePassword', 'UserController@changePassword');

Route::post('changeDetail', 'UserController@changeDetail');

Route::get('requestList', 'BookingsController@requestList');

Route::post('acceptRequest', 'BookingsController@acceptRequest');

Route::post('rejectRequest', 'BookingsController@rejectRequest');

Route::post('deleteLab', 'LabController@deleteLab');