<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = ['user_id','lab_id', 'description', 'book_date','book_time','status'];
}
