<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Booking;
use Illuminate\Support\Facades\Redis;

class UserController extends Controller
{

    public function index(){
        if(Auth::check()){
            if(Auth::user()->role === 0){
                $users = User::all()->sortBy('name');
                return view('userList',compact('users'));
            }
            elseif(Auth::user()->role === 1){
                $users = User::where('role','=',2)->get()->sortBy('name');
                return view('userList',compact('users'));
            }
            else{
                return redirect()->to('admin-login');
            }

            
        }
        else{
            return redirect()->to('admin-login');
        }
    }

    public function welcome(){
        if(Auth::check()){
            if(Auth::user()->role < 2){
                return redirect('sapsp-admin');
            }
            else{
                return redirect('home');
            }
        }
        else{
            if(User::where('role','=',0)->exists()){
                return view('welcome');
            }
            else{
                return redirect('super-admin-create');
            }
        }
    }

    public function resetUser(Request $request){
        $id = $request->id;
        $resetPassword = Hash::make('0000');
        User::where('id','=',$id)
            ->update(['password'=>$resetPassword]);

        return 'success';
    }

    public function deleteUser(Request $request){
        $id = $request->id;
        User::where('id','=',$id)->delete();
        if(Booking::where('user_id','=',$id)->exists()){
            Booking::where('user_id','=',$id)->delete();
        }

        return 'success';
    }

    public function addAdmin(Request $request){
        $id = $request->id;
        $name = $request->name;
        $email = $request->email;
        $contact = $request->contact;
        $role = $request->role;
        // $roleVal = 1;
        // if($role === 'Super Admin'){
        //     $roleVal = 0;
        // }
        $password = $request->password;
        $checkPassword = $request->checkPassword;
        $hashed = Hash::make($password);

        if(User::where('id','=',$id)->exists()){
            return 'exist';
        }
        else{
            if($password != $checkPassword){
                return 'different'.$password.' '.$checkPassword;
            }
            else{
                User::create(['id'=>$id,'name'=>$name,'email'=>$email,'contact'=>$contact,'password'=>$hashed,'role'=>$role]);
                return 'success';
            }
        }
        
    }

    public function registerStudent()
    {

        $this->validate(request(), [
            'id' => 'required',
            'name' => 'required',
            'password' => 'required',
            'email' => 'required'
        ]);

        $id = request('id');
        $name = request('name');
        $password = request('password');
        $email = request('email');
        $contact = request('contact');
        $hashed = Hash::make($password);

        //check if id existed
        if(User::where('id','=',$id)->first()){
            $existed = "Matrix no already registered";
            return view('welcome',compact('existed'));
        }
        else{
            $user = User::create(['id'=>$id,'name'=>$name,'password'=>$hashed,'email'=>$email,'contact'=>$contact,'role'=>2]);
            auth()->login($user);

            return redirect()->to('/home');
        }


    }

    public function loginStudent(Request $request)
    {

        $id = $request->input('id');
        $password = $request->input('password');
        $hashedPassword = Hash::make($password);


        $user = User::where('id', '=', $id)->first();
        if (User::where('id', '=', $id)->exists()) {
            if(Hash::check($password, $user->password)){
                Auth::login($user);
                return redirect()->to('/home');
            }
            else{
                return redirect()->back()->with('alert','Invalid ID or password')->withInput();
            }
         }
         else{
             echo "user not found";
             return redirect()->back()->with('alert','Invalid ID or password')->withInput();
         }
    }

    public function adminLogin(Request $request){
        $id = $request->id;
        $password = $request->password;
        $invalidUserPass = 'Invalid ID or Password';
        $notAdmin = 'Please use admin account to login from this page';

        $user = User::where('id','=',$id)->first();
        if(User::where('id', '=', $id)->exists()){
            if(Hash::check($password, $user->password)){
                if($user->role < 2){
                    Auth::login($user);
                    return redirect()->to('sapsp-admin');
                }
                else{
                    return redirect()->back()->with('alert', $notAdmin);
                }
            }
            else{
                return redirect()->back()->with('alert',$invalidUserPass);
            }
        }
        else{
            return redirect()->back()->with('alert',$invalidUserPass);
        }
    }

    public function userLogout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function adminLogout()
    {
        Auth::logout();
        return redirect('admin-login');
    }

    public function superAdminCreateIndex(){
        if(User::where('role', '=', 0)->exists()){
            return redirect('admin-login');
        }
        else{
            return view('superAdmin');
        }
    }

    public function superAdminRegister(Request $request){
        $id = $request->id;
        $name = $request->name;
        $email = $request->email;
        $contact = $request->contact;
        $password = $request->password;
        $hashed = Hash::make($password);

        if(User::where('id','=',$id)->exists()){
            return redirect()->back()->with('alerts','Input ID already exist')->withInput();
        }
        else{
            $user = User::create(['id'=>$id,'name'=>$name,'email'=>$email,'contact'=>$contact,'password'=>$hashed,'role'=>0]);
            Auth::login($user);
            return redirect('sapsp-admin');
        }
    }

    public function search(Request $request){
        $id = $request->id;
        if(Auth::user()->role == 0){
            $users = User::where('id','like','%'.$id.'%')->orderBy('name')->get(); 
            $data = json_encode($users);
            return $data;
        }
        elseif(Auth::user()->role == 1){
            $users = User::where('id','like','%'.$id.'%')->where('role','=',2)->orderBy('name')->get(); 
            $data = json_encode($users);
            return $data;
        }
        else{
            return redirect('/');
        }

    }

    public function userSetting(){
        if(Auth::check()){
            if(Auth::user()->role == 2){
                $user = User::where('id','=',Auth::user()->id)->first();

                return view('userSetting',compact('user'));
            }
            else{
                $user = User::where('id','=',Auth::user()->id)->first();

                return view('adminSetting',compact('user'));
            }
        }
        else {
            return redirect('/');
        }
    }

    public function changePassword(Request $request){
        if(Auth::check()){
            $old = $request->oldPassword;
            $new = $request->newPassword;
            $hashed = Hash::make($new);
            $re = $request->rePassword;
            $user = User::where('id','=',Auth::user()->id)->first();

            if(User::where('id','=',Auth::user()->id)->exists()){
                if(Hash::check($old, $user->password)){
                    if($new === $re){
                        User::where('id','=',Auth::user()->id)->update(['password'=>$hashed]);
                        
                        return redirect()->back()->with('alert','Successfully changed password');
                        
                    }
                    else {
                        return redirect()->back()->with('alert','New password is not same with retype password')->withInput();
                    }
                }
                else{
                    return redirect()->back()->with('alert','Invalid old password')->withInput();
                }
            }
            else{
                return redirect('/');
            }
        }
        else{
            return redirect('/');
        }
    }

    public function changeDetail(Request $request){
        if(Auth::check()){
            $name = $request->name;
            $email = $request->email;
            $contact = $request->contact;

            if(User::where('id','=',Auth::user()->id)->exists()){
                User::where('id','=',Auth::user()->id)->update(['name'=>$name,'email'=>$email,'contact'=>$contact]);
                return redirect()->back()->with('alert','Successfully updated details');
            }
            else{
                return redirect('/');
            }
        }
        else{
            return redirect('/');
        }
    }
}
