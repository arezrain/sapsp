<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;
use App\Booking;
use App\Lab;
use Carbon\Carbon;
use DB;

class BookingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() //to initialize booking page and diffrentiate user role
    {
        if(Auth::check()){
            $labs = Lab::all();
            $bookings = Booking::all();
            $timeList = ["08.00 - 09.00","09.00 - 10.00","10.00 - 11.00","11.00 - 12.00","12.00 - 13.00","13.00 - 14.00","14.00 - 15.00","15.00 - 16.00","16.00 - 17.00"];
            
            if(Auth::user()->role == 2){// if user is student return booking.blade.php
                return view('booking', compact('bookings','labs','timeList'));
            }
            else if(Auth::user()->role == 0 || Auth::user()->id == 1){// if user is admin return adminBooking.blade.php
                return view('adminBooking', compact('bookings','labs','timeList'));
            }
        }
        else{
            return redirect('/');
        }
    }

    public function adminHome(){// to intialize admin home page
        if(Auth::check()){
            $month = Carbon::now()->addHours(8)->month;
            $year = Carbon::now()->addHours(8)->year;
            $today = Carbon::now()->addHours(8)->day;
            $labIdArr = array();
            if($month < 10){
                $todayString = $year.'-0'.$month;
            }
            else{
                $todayString = $year.'-'.$month;
            }

            $labs = Lab::all();

            $t = Carbon::now()->addHours(8)->daysInMonth;
            $date = $year.'-'.$month.'-1';
            $day = Carbon::parse($date)->format('l');
            $count = Lab::count();
            foreach($labs as $lab){
                array_push($labIdArr,$lab->id);
            }
            $bookings = Booking::where('book_date','like', '%'.$todayString.'%')->get();
            return view('adminHome',compact('bookings','labs','count','labIdArr'));
        }
        else{
            return redirect()->to('admin-login');
        }

    }

    public function userHome(){
        if(Auth::check()){
            $userId = Auth::id();
            $bookings = DB::table('bookings')->join('labs','bookings.lab_id','=','labs.id')
            ->select('bookings.id','labs.name','bookings.description','bookings.book_date','bookings.book_time','bookings.status')
            ->where('bookings.user_id','=',$userId)
            ->orderBy('bookings.book_date')
            ->orderBy('bookings.book_time')->get();

            $histories = DB::table('bookings')->join('labs','bookings.lab_id','=','labs.id')
            ->select('bookings.id','labs.name','bookings.description','bookings.book_date','bookings.book_time')
            ->where('bookings.user_id','=',$userId)
            ->orderBy('bookings.book_date','desc')
            ->orderBy('bookings.book_time','desc')->get();
            return view('home',compact('bookings','histories'));
        }
        else{
            return view('welcome');
        }
    }

    public function initialMonthlySummary(){

        if(Auth::check()){
            if(Auth::user()->role < 2){
                $month = Carbon::now()->addHours(8)->month;
                $year = Carbon::now()->addHours(8)->year;
                $today = Carbon::now()->addHours(8)->day;

                if($month < 10){
                    $month = '0'.$month;
                }

                if($today < 10){
                    $today = '0'.$today;
                }

                $labCount = DB::select("SELECT bookings.lab_id, COUNT(bookings.lab_id) AS 'total', labs.name
                                        FROM bookings
                                        INNER JOIN labs ON bookings.lab_id = labs.id
                                        WHERE bookings.book_date LIKE '$year-$month%'
                                        AND bookings.status = 'approved'
                                        GROUP BY bookings.lab_id");

                $listBasedLab = DB::select("SELECT bookings.id, labs.name, users.name AS 'user_name', users.contact,
                                            bookings.description, bookings.book_date, bookings.book_time
                                            FROM bookings
                                            INNER JOIN users ON bookings.user_id=users.id
                                            INNER JOIN labs ON bookings.lab_id=labs.id
                                            WHERE bookings.book_date LIKE '$year-$month%'
                                            AND bookings.status = 'approved'
                                            ORDER BY bookings.lab_id");

                $response = [
                    'labcount' => $labCount,
                    'listlab' => $listBasedLab
                ];

                $jsonList = json_encode($response);

                return view('adminLog',compact('labCount','listBasedLab','jsonList'));
            }
            else{
                return redirect('/');
            }
        }
        else{
            return redirect('/');
        }

        
    }

    public function selectedDateLab(Request $request){
        if($request->ajax())
        {
            $labId = Lab::where('name','=',$request->lab)->first();
            $bookings = Booking::where('book_date','=',$request->date)->where('lab_id','=',$labId->id)
                        ->where('status','!=','rejected')->get('book_time');

            return $bookings->toJson();
        }
        else{
            return redirect()->route('bookings.index');
        }

    }

    public function selectedMonth(Request $request){
        if($request->ajax())
        {
            $month = $request->month;
            $year = $request->year;

            if($month < 10){
                $todayString = $year.'-0'.$month;
            }
            else{
                $todayString = $year.'-'.$month;
            }
            $bookings = Booking::select('lab_id','book_date','description')
                                ->where('book_date','like', $todayString.'%')
                                ->where('status','=','approved')->get();

            return $bookings->toJson();
        }
        else{
            return redirect()->route('bookings.index');
        }

    }

    public function selectedDateEvent(Request $request){
        if($request->ajax())
        {
            $bookings = Booking::where('book_date','=',$request->date)->where('status','=','approved')->get();


            return $bookings->toJson();
        }
        else
        {
            return redirect()->route('bookings.adminHome');
        }

    }

    public function cancelBooking(Request $request){
        if($request->ajax())
        {
            Booking::destroy($request->id);


            return "success";
        }
        else{
            return view('welcome');
        }

    }

    public function clearHistory(Request $request){
        if($request->ajax())
        {
            $month = Carbon::now()->addHours(8)->month;
            $year = Carbon::now()->addHours(8)->year;
            $today = Carbon::now()->addHours(8)->day;
            $todayString = $year.'-'.$month.'-'.$today;
            $now = $request->date;
            $userId = Auth::id();
            Booking::where('user_id','=',$userId)
                    ->where('book_date','<',$todayString)
                    ->delete();

            return "success";
        }
        else{
            return view('welcome');
        }


    }

    public function create()
    {
        return view('tasks.create');
    }

    public function store(Request $request)
    {
        $lab = $request->lab_name;
        $id = $request->user_id;
        $name = $request->user_name;
        $date = $request->book_date;
        $time = $request->book_time;
        $users = DB::select("SELECT *
                    FROM users
                    WHERE role < 2");

        $labId = Lab::where('name','=',$lab)->first();
        if(Auth::user()->role == 2){
            Booking::create(['user_id'=>$id,'lab_id'=>$labId->id,'description'=>$request->description,'book_date'=>$date,'book_time'=>$time,'status'=>'pending']);
        }
        else{
            Booking::create(['user_id'=>$id,'lab_id'=>$labId->id,'description'=>$request->description,'book_date'=>$date,'book_time'=>$time,'status'=>'approved']);
        }
        $data = "User ID: ".$id."\nLab ID: ".$labId->id."\nDate: ".$request->book_date."\nTime: ".$request->book_time."\nDescription: ".$request->description;
        $time = substr($request->book_time,0,2);

        return $time;
    }

    public function daySummary(Request $request){
        $date = $request->date;
        $monthDate = substr($date,0,7);
        $data = array();
        $myString = 'sugoi ne';

        $labCount = DB::select("SELECT bookings.lab_id, COUNT(bookings.lab_id) AS 'total', labs.name
                                FROM bookings
                                INNER JOIN labs ON bookings.lab_id = labs.id
                                WHERE bookings.book_date LIKE '$date%'
                                AND bookings.status = 'approved'
                                GROUP BY bookings.lab_id");

        $listBasedLab = DB::select("SELECT bookings.id, labs.name, users.name AS 'user_name', users.contact, 
                                    bookings.description, bookings.book_date, bookings.book_time
                                    FROM bookings
                                    INNER JOIN users ON bookings.user_id=users.id
                                    INNER JOIN labs ON bookings.lab_id=labs.id
                                    WHERE bookings.book_date LIKE '$date%'
                                    AND bookings.status = 'approved'
                                    ORDER BY bookings.lab_id");

        $data['labCount'] = $labCount;
        $data['listBasedLab'] = $listBasedLab;

        $response = [
            'labcount' => $labCount,
            'listlab' => $listBasedLab
        ];


        return response()->json($response);

    }

    public function requestPage(){
        if(Auth::check()){
            if(Auth::user()->role == 2){
                return redirect('/');
            }
            else{
                return view('adminRequest');
            }
        }
        else {
            return redirect('admin-login');
        }

        
    }

    public function requestList(){
        if(Auth::check()){
            if(Auth::user()->role < 2){
                $bookings = DB::select("SELECT bookings.id, labs.name AS 'lab_name', users.name AS 'user_name', users.contact, 
                                bookings.description, bookings.book_date, bookings.book_time
                                FROM bookings
                                INNER JOIN users ON bookings.user_id=users.id
                                INNER JOIN labs ON bookings.lab_id=labs.id
                                WHERE bookings.status = 'pending'
                                ORDER BY bookings.book_date, bookings.book_time");

                return $bookings;
            }
            else{
                return redirect('/');
            }
        }
        else {
            return redirect('admin-login');
        }
        
    }

    public function acceptRequest(Request $request){
        $id = $request->id;

        Booking::where('id','=',$id)->update(['status'=>'approved']);
        return 'success';
    }

    public function rejectRequest(Request $request){
        $id = $request->id;

        Booking::where('id','=',$id)->update(['status'=>'rejected']);
        return 'success';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
