<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lab;
use App\Booking;
use DB;

class LabController extends Controller
{
    public function index(){
        $labs = Lab::all();
        return view('adminLab',compact('labs')); //return adminLab.blade.php with all lab data
    }

    public function editLab(Request $request){
        $id = $request->newId;
        $name = $request->newName;
        $oldId = $request->oldId;

        Lab::where('id','=',$oldId)
            ->update(['id'=>$id,'name'=>$name]);

        Booking::where('lab_id','=',$oldId)
            ->update(['lab_id'=>$id]);

        return 'success';
    }

    public function addLab(Request $request){
        $name = $request->newName;
        $lab = Lab::create(['name'=>$name]);
        return 'success';
    }

    public function deleteLab(Request $request){
        $id = $request->id;
        Lab::where('id','=',$id)->delete();

        Booking::where('lab_id','=',$id)->delete();

        return 'success';
    }
}
