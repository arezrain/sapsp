<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use DB;
use App\User;

class MailSend extends Controller
{
    public function mailsend(Request $request)
    {
        $lab = $request->lab;
        $name = $request->name;
        $id = $request->id;
        $contact = Auth::user()->contact;
        $email = Auth::user()->email;
        $date = $request->date;
        $time = $request->time;
        
        $users = DB::select("SELECT *
                    FROM users
                    WHERE role < 2");

        $details = [
            'title' =>   'Booking Notification',
            'body' =>    'New lab booking has been made at '.now()->addHours(8),
            'lab' =>     'Lab/Room : '.$lab,
            'id' =>      'User ID  : '.$id,
            'name' =>    'Name     : '.$name,
            'contact' => 'Contact  : '.$contact,
            'email' =>   'Email    : '.$email,
            'date' =>    'Date     : '.$date,
            'time' =>    'Time     : '.$time,

        ];

        foreach($users as $user){
            Mail::to($user->email)->send(new SendMail($details));
        }
        return 'success';
    }
}
